(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-account-order-details-order-details-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/account/order-details/order-details.page.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/account/order-details/order-details.page.html ***!
  \***********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button mode=\"md\" color=\"primary\"></ion-back-button>\n    </ion-buttons>\n    <ion-title color=\"primary\">Order Detail</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-card style=\"position: relative;\">\n    <ion-card-header>\n      <ion-label>Ordered ID <span>2513253423 </span></ion-label>\n\n      <ion-label>Placed on <span>28/12/2016</span></ion-label>\n\n      <ion-label> Return Product</ion-label>\n\n    </ion-card-header>\n\n    <ion-card-content>\n      <ion-row class=\"no-padding\">\n        <ion-col size=\"7\" class=\"first_col\">\n          <h4>Product 2</h4>\n          <ion-label class=\"gray_lbl\">Quantity : 1</ion-label>\n\n          <ion-label class=\"price\">500$</ion-label>\n\n          <ion-label class=\"gray_lbl\">via Credit Card</ion-label>\n          <ion-label class=\"gray_lbl\">Tracking Status on</ion-label>\n          <ion-label class=\"gray_lbl\">Delivered on <span style=\"font-weight: 600;\">30/12/2017</span> </ion-label>\n\n          <ion-button (click)=\"onClick()\" class=\"btn\" style=\"color: white;\">\n            <ion-icon slot=\"start\" name=\"arrow-down\"></ion-icon>Recieved\n          </ion-button>\n\n        </ion-col>\n        <ion-col size=\"5\">\n          <div class=\"img-box\">\n            <img src=\"assets/products/1.jpg\">\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-card-content>\n  </ion-card>\n\n  <div class=\"tracking_div\">\n      \n    <div class=\"left\">\n      <span *ngFor=\"let item of orderDetail\">\n        <div class=\"line_div\" [class.line_div_darkgray]=\"item.status == 1\"></div>\n        <div class=\"round_div_gray\" [class.round_div_red]=\"item.status == 2\" [class.round_div_darkgray]=\"item.status == 1\"></div>\n      </span>\n    </div>\n    \n    <div class=\"right\">\n      <span *ngFor=\"let item of orderDetail\">\n        <div class=\"line_div\"></div>\n        <div class=\"round_div_gray\" [class.round_div_red]=\"item.status == 2\" [class.round_div_darkgray]=\"item.status == 1\">\n          <ion-label>{{item.time}} - {{item.date}}</ion-label>\n          <ion-label>{{item.value}}</ion-label>\n        </div>\n      </span>\n    </div>\n\n  </div>\n\n</ion-content>\n");

/***/ }),

/***/ "./src/app/pages/account/order-details/order-details.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/account/order-details/order-details.module.ts ***!
  \*********************************************************************/
/*! exports provided: OrderDetailsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderDetailsPageModule", function() { return OrderDetailsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _order_details_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./order-details.page */ "./src/app/pages/account/order-details/order-details.page.ts");







const routes = [
    {
        path: '',
        component: _order_details_page__WEBPACK_IMPORTED_MODULE_6__["OrderDetailsPage"]
    }
];
let OrderDetailsPageModule = class OrderDetailsPageModule {
};
OrderDetailsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_order_details_page__WEBPACK_IMPORTED_MODULE_6__["OrderDetailsPage"]]
    })
], OrderDetailsPageModule);



/***/ }),

/***/ "./src/app/pages/account/order-details/order-details.page.scss":
/*!*********************************************************************!*\
  !*** ./src/app/pages/account/order-details/order-details.page.scss ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-label {\n  display: block;\n}\n\nion-card-header {\n  padding: 10px;\n  position: relative;\n}\n\nion-card-header ion-label {\n  margin-bottom: 10px;\n}\n\nion-card-header span {\n  color: #999 !important;\n  font-weight: 600;\n}\n\nion-card-content {\n  padding: 10px;\n}\n\nion-card-content .first_col {\n  padding: 0px !important;\n}\n\nion-card-content ion-label {\n  margin-bottom: 10px;\n}\n\nion-card-content .gray_lbl {\n  color: #999;\n}\n\nion-card-content .price {\n  font-weight: 600;\n}\n\nion-card-content ion-button {\n  --border-radius: 5px;\n}\n\n.tracking_div {\n  margin-top: 30px;\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n          flex-direction: row;\n  background: #FAFAFA;\n  padding-bottom: 25px;\n}\n\n.tracking_div .left {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n          flex-direction: column;\n  -webkit-box-align: center;\n          align-items: center;\n  width: 25%;\n}\n\n.tracking_div .left .line_div {\n  height: 40px;\n  width: 3px;\n  border: 2px solid #E8E8E8;\n}\n\n.tracking_div .left .line_div_darkgray {\n  height: 40px;\n  width: 3px;\n  border: 2px solid #C8C7CE;\n}\n\n.tracking_div .left .round_div_gray {\n  height: 18px;\n  width: 18px;\n  background-color: #E8E8E8;\n  border-radius: 50%;\n  margin-left: -7px;\n}\n\n.tracking_div .left .round_div_darkgray {\n  height: 18px;\n  width: 18px;\n  background-color: #C8C7CE;\n  border-radius: 50%;\n  margin-left: -7px;\n}\n\n.tracking_div .left .round_div_red {\n  height: 18px;\n  width: 18px;\n  background-color: #3b6098;\n  border-radius: 50%;\n  margin-left: -7px;\n}\n\n.tracking_div .right {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n          flex-direction: column;\n  -webkit-box-align: start;\n          align-items: flex-start;\n  width: 75%;\n}\n\n.tracking_div .right ion-label {\n  font-size: 14px;\n  display: block;\n}\n\n.tracking_div .right .line_div {\n  height: 40px;\n  width: 100%;\n}\n\n.tracking_div .right .round_div_gray {\n  height: 20px;\n  width: 100% px;\n  color: #E8E8E8;\n  margin-top: -4px;\n}\n\n.tracking_div .right .round_div_red {\n  height: 20px;\n  width: 100% px;\n  color: #3b6098;\n}\n\n.tracking_div .right .round_div_darkgray {\n  height: 20px;\n  width: 100%;\n  color: #C8C7CE;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYWNjb3VudC9vcmRlci1kZXRhaWxzL0M6XFxVc2Vyc1xcVXNlclxcRG93bmxvYWRzXFxpb25pYzVTaG9wQXBwLTV5bHBkY1xcaW9uaWM1U2hvcEFwcFxcQXBwX2NvZGUvc3JjXFxhcHBcXHBhZ2VzXFxhY2NvdW50XFxvcmRlci1kZXRhaWxzXFxvcmRlci1kZXRhaWxzLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvYWNjb3VudC9vcmRlci1kZXRhaWxzL29yZGVyLWRldGFpbHMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksY0FBQTtBQ0NKOztBRENBO0VBQ0ksYUFBQTtFQUNBLGtCQUFBO0FDRUo7O0FEREk7RUFDSSxtQkFBQTtBQ0dSOztBRERJO0VBQ0ksc0JBQUE7RUFDQSxnQkFBQTtBQ0dSOztBRENBO0VBQ0ksYUFBQTtBQ0VKOztBREFJO0VBQ0ksdUJBQUE7QUNFUjs7QURDSTtFQUNJLG1CQUFBO0FDQ1I7O0FERUk7RUFDSSxXQUFBO0FDQVI7O0FERUk7RUFDSSxnQkFBQTtBQ0FSOztBREVJO0VBQ0ksb0JBQUE7QUNBUjs7QURJQTtFQUNJLGdCQUFBO0VBQ0Esb0JBQUE7RUFBQSxhQUFBO0VBQ0EsOEJBQUE7RUFBQSw2QkFBQTtVQUFBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxvQkFBQTtBQ0RKOztBREdJO0VBQ0ksb0JBQUE7RUFBQSxhQUFBO0VBQ0EsNEJBQUE7RUFBQSw2QkFBQTtVQUFBLHNCQUFBO0VBQ0EseUJBQUE7VUFBQSxtQkFBQTtFQUNBLFVBQUE7QUNEUjs7QURFUTtFQUNJLFlBQUE7RUFDQSxVQUFBO0VBQ0EseUJBQUE7QUNBWjs7QURHUTtFQUNJLFlBQUE7RUFDQSxVQUFBO0VBQ0EseUJBQUE7QUNEWjs7QURJUTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0EseUJBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0FDRlo7O0FESVE7RUFDSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtBQ0ZaOztBRElRO0VBQ0ksWUFBQTtFQUNBLFdBQUE7RUFDQSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7QUNGWjs7QURNSTtFQUNJLG9CQUFBO0VBQUEsYUFBQTtFQUNBLDRCQUFBO0VBQUEsNkJBQUE7VUFBQSxzQkFBQTtFQUNBLHdCQUFBO1VBQUEsdUJBQUE7RUFDQSxVQUFBO0FDSlI7O0FETVE7RUFDSSxlQUFBO0VBQ0EsY0FBQTtBQ0paOztBRE1RO0VBQ0ksWUFBQTtFQUNBLFdBQUE7QUNKWjs7QURNUTtFQUNJLFlBQUE7RUFDQSxjQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0FDSlo7O0FETVE7RUFDSSxZQUFBO0VBQ0EsY0FBQTtFQUNBLGNBQUE7QUNKWjs7QURNUTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0EsY0FBQTtBQ0paIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvYWNjb3VudC9vcmRlci1kZXRhaWxzL29yZGVyLWRldGFpbHMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWxhYmVse1xuICAgIGRpc3BsYXk6IGJsb2NrO1xufVxuaW9uLWNhcmQtaGVhZGVye1xuICAgIHBhZGRpbmc6IDEwcHg7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGlvbi1sYWJlbCB7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gICAgfVxuICAgIHNwYW4ge1xuICAgICAgICBjb2xvcjogIzk5OSAhaW1wb3J0YW50OyAgICAgICAgXG4gICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgfVxufVxuXG5pb24tY2FyZC1jb250ZW50e1xuICAgIHBhZGRpbmc6IDEwcHg7XG5cbiAgICAuZmlyc3RfY29se1xuICAgICAgICBwYWRkaW5nOiAwcHggIWltcG9ydGFudDtcbiAgICB9XG4gICAgXG4gICAgaW9uLWxhYmVsIHtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgICB9XG5cbiAgICAuZ3JheV9sYmx7XG4gICAgICAgIGNvbG9yOiAjOTk5O1xuICAgIH1cbiAgICAucHJpY2V7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgfVxuICAgIGlvbi1idXR0b257XG4gICAgICAgIC0tYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIH1cbn1cblxuLnRyYWNraW5nX2RpdntcbiAgICBtYXJnaW4tdG9wOiAzMHB4O1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBiYWNrZ3JvdW5kOiAjRkFGQUZBO1xuICAgIHBhZGRpbmctYm90dG9tOiAyNXB4O1xuXG4gICAgLmxlZnR7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIHdpZHRoOiAyNSU7XG4gICAgICAgIC5saW5lX2RpdntcbiAgICAgICAgICAgIGhlaWdodDogNDBweDtcbiAgICAgICAgICAgIHdpZHRoOiAzcHg7XG4gICAgICAgICAgICBib3JkZXI6IDJweCBzb2xpZCAjRThFOEU4O1xuICAgICAgICB9XG5cbiAgICAgICAgLmxpbmVfZGl2X2RhcmtncmF5e1xuICAgICAgICAgICAgaGVpZ2h0OiA0MHB4O1xuICAgICAgICAgICAgd2lkdGg6IDNweDtcbiAgICAgICAgICAgIGJvcmRlcjogMnB4IHNvbGlkICNDOEM3Q0U7XG4gICAgICAgIH1cblxuICAgICAgICAucm91bmRfZGl2X2dyYXl7XG4gICAgICAgICAgICBoZWlnaHQ6IDE4cHg7XG4gICAgICAgICAgICB3aWR0aDogMThweDtcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNFOEU4RTg7XG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgICAgICAgICBtYXJnaW4tbGVmdDogLTdweDtcbiAgICAgICAgfVxuICAgICAgICAucm91bmRfZGl2X2RhcmtncmF5e1xuICAgICAgICAgICAgaGVpZ2h0OiAxOHB4O1xuICAgICAgICAgICAgd2lkdGg6IDE4cHg7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjQzhDN0NFO1xuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IC03cHg7XG4gICAgICAgIH1cbiAgICAgICAgLnJvdW5kX2Rpdl9yZWR7XG4gICAgICAgICAgICBoZWlnaHQ6IDE4cHg7XG4gICAgICAgICAgICB3aWR0aDogMThweDtcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICMzYjYwOTg7XG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgICAgICAgICBtYXJnaW4tbGVmdDogLTdweDtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIC5yaWdodHtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XG4gICAgICAgIHdpZHRoOiA3NSU7XG5cbiAgICAgICAgaW9uLWxhYmVsIHtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICB9XG4gICAgICAgIC5saW5lX2RpdntcbiAgICAgICAgICAgIGhlaWdodDogNDBweDtcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICB9XG4gICAgICAgIC5yb3VuZF9kaXZfZ3JheXtcbiAgICAgICAgICAgIGhlaWdodDogMjBweDtcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlcHg7XG4gICAgICAgICAgICBjb2xvcjogI0U4RThFODtcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IC00cHg7XG4gICAgICAgIH1cbiAgICAgICAgLnJvdW5kX2Rpdl9yZWR7XG4gICAgICAgICAgICBoZWlnaHQ6IDIwcHg7XG4gICAgICAgICAgICB3aWR0aDogMTAwJXB4O1xuICAgICAgICAgICAgY29sb3I6ICMzYjYwOTg7XG4gICAgICAgIH1cbiAgICAgICAgLnJvdW5kX2Rpdl9kYXJrZ3JheXtcbiAgICAgICAgICAgIGhlaWdodDogMjBweDtcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAgICAgY29sb3I6ICNDOEM3Q0U7XG4gICAgICAgIH1cbiAgICB9XG59IiwiaW9uLWxhYmVsIHtcbiAgZGlzcGxheTogYmxvY2s7XG59XG5cbmlvbi1jYXJkLWhlYWRlciB7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbmlvbi1jYXJkLWhlYWRlciBpb24tbGFiZWwge1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xufVxuaW9uLWNhcmQtaGVhZGVyIHNwYW4ge1xuICBjb2xvcjogIzk5OSAhaW1wb3J0YW50O1xuICBmb250LXdlaWdodDogNjAwO1xufVxuXG5pb24tY2FyZC1jb250ZW50IHtcbiAgcGFkZGluZzogMTBweDtcbn1cbmlvbi1jYXJkLWNvbnRlbnQgLmZpcnN0X2NvbCB7XG4gIHBhZGRpbmc6IDBweCAhaW1wb3J0YW50O1xufVxuaW9uLWNhcmQtY29udGVudCBpb24tbGFiZWwge1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xufVxuaW9uLWNhcmQtY29udGVudCAuZ3JheV9sYmwge1xuICBjb2xvcjogIzk5OTtcbn1cbmlvbi1jYXJkLWNvbnRlbnQgLnByaWNlIHtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cbmlvbi1jYXJkLWNvbnRlbnQgaW9uLWJ1dHRvbiB7XG4gIC0tYm9yZGVyLXJhZGl1czogNXB4O1xufVxuXG4udHJhY2tpbmdfZGl2IHtcbiAgbWFyZ2luLXRvcDogMzBweDtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgYmFja2dyb3VuZDogI0ZBRkFGQTtcbiAgcGFkZGluZy1ib3R0b206IDI1cHg7XG59XG4udHJhY2tpbmdfZGl2IC5sZWZ0IHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgd2lkdGg6IDI1JTtcbn1cbi50cmFja2luZ19kaXYgLmxlZnQgLmxpbmVfZGl2IHtcbiAgaGVpZ2h0OiA0MHB4O1xuICB3aWR0aDogM3B4O1xuICBib3JkZXI6IDJweCBzb2xpZCAjRThFOEU4O1xufVxuLnRyYWNraW5nX2RpdiAubGVmdCAubGluZV9kaXZfZGFya2dyYXkge1xuICBoZWlnaHQ6IDQwcHg7XG4gIHdpZHRoOiAzcHg7XG4gIGJvcmRlcjogMnB4IHNvbGlkICNDOEM3Q0U7XG59XG4udHJhY2tpbmdfZGl2IC5sZWZ0IC5yb3VuZF9kaXZfZ3JheSB7XG4gIGhlaWdodDogMThweDtcbiAgd2lkdGg6IDE4cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICNFOEU4RTg7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgbWFyZ2luLWxlZnQ6IC03cHg7XG59XG4udHJhY2tpbmdfZGl2IC5sZWZ0IC5yb3VuZF9kaXZfZGFya2dyYXkge1xuICBoZWlnaHQ6IDE4cHg7XG4gIHdpZHRoOiAxOHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjQzhDN0NFO1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIG1hcmdpbi1sZWZ0OiAtN3B4O1xufVxuLnRyYWNraW5nX2RpdiAubGVmdCAucm91bmRfZGl2X3JlZCB7XG4gIGhlaWdodDogMThweDtcbiAgd2lkdGg6IDE4cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICMzYjYwOTg7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgbWFyZ2luLWxlZnQ6IC03cHg7XG59XG4udHJhY2tpbmdfZGl2IC5yaWdodCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xuICB3aWR0aDogNzUlO1xufVxuLnRyYWNraW5nX2RpdiAucmlnaHQgaW9uLWxhYmVsIHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBkaXNwbGF5OiBibG9jaztcbn1cbi50cmFja2luZ19kaXYgLnJpZ2h0IC5saW5lX2RpdiB7XG4gIGhlaWdodDogNDBweDtcbiAgd2lkdGg6IDEwMCU7XG59XG4udHJhY2tpbmdfZGl2IC5yaWdodCAucm91bmRfZGl2X2dyYXkge1xuICBoZWlnaHQ6IDIwcHg7XG4gIHdpZHRoOiAxMDAlIHB4O1xuICBjb2xvcjogI0U4RThFODtcbiAgbWFyZ2luLXRvcDogLTRweDtcbn1cbi50cmFja2luZ19kaXYgLnJpZ2h0IC5yb3VuZF9kaXZfcmVkIHtcbiAgaGVpZ2h0OiAyMHB4O1xuICB3aWR0aDogMTAwJSBweDtcbiAgY29sb3I6ICMzYjYwOTg7XG59XG4udHJhY2tpbmdfZGl2IC5yaWdodCAucm91bmRfZGl2X2RhcmtncmF5IHtcbiAgaGVpZ2h0OiAyMHB4O1xuICB3aWR0aDogMTAwJTtcbiAgY29sb3I6ICNDOEM3Q0U7XG59Il19 */");

/***/ }),

/***/ "./src/app/pages/account/order-details/order-details.page.ts":
/*!*******************************************************************!*\
  !*** ./src/app/pages/account/order-details/order-details.page.ts ***!
  \*******************************************************************/
/*! exports provided: OrderDetailsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderDetailsPage", function() { return OrderDetailsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");

/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/

let OrderDetailsPage = class OrderDetailsPage {
    constructor() {
        this.orderDetail = [
            {
                status: 1,
                value: 'Order Confirmed',
                time: '12:00 PM',
                date: '12/12/2017'
            },
            {
                status: 1,
                value: 'Prepering Your Order',
                time: '10:00 AM',
                date: '13/12/2017'
            },
            {
                status: 2,
                value: 'Rider is picking up your order',
                time: '05:00 PM',
                date: '17/12/2017'
            },
            {
                status: 0,
                value: 'Rider is near by at your place',
                time: '06:00 PM',
                date: '19/12/2017'
            },
        ];
    }
    ngOnInit() {
    }
    onClick() {
    }
};
OrderDetailsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-order-details',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./order-details.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/account/order-details/order-details.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./order-details.page.scss */ "./src/app/pages/account/order-details/order-details.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], OrderDetailsPage);



/***/ })

}]);
//# sourceMappingURL=pages-account-order-details-order-details-module-es2015.js.map