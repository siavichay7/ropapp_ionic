function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-product-product-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/product/product.page.html":
  /*!***************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/product/product.page.html ***!
    \***************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesProductProductPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button mode=\"md\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>\n      Productos\n    </ion-title>\n    <ion-icon slot=\"end\" (click)=\"onCart()\" class=\"header_icons\" name=\"basket\"></ion-icon>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-slides pager=\"true\">\n    <ion-slide *ngFor=\"let slide of images\" style=\"height: 100% !important; width: 100%;padding: 15px;\">\n      <div class=\"div_img_back\" [style.backgroundImage]=\"'url('+slide+')'\">\n      </div>\n    </ion-slide>\n  </ion-slides>\n  <div class=\"details\">\n\n    <div class=\"prod_main\">\n      <ion-label class=\"title\">Adidas</ion-label>\n      <ion-label class=\"sub_t\">Men Navy Blue</ion-label>\n      <ion-label class=\"feature\">Loafers</ion-label>\n      <ion-label class=\"prod_orgin\">$22.2 </ion-label>&nbsp; <span class=\"prod_sell\">$10.2</span>\n    </div>\n\n    <div class=\"prod_main\">\n      <ion-label class=\"title\">Facilidad de 30 días e intercambio</ion-label>\n      <ion-label class=\"sub_t\"> Escoger.</ion-label>\n    </div>\n\n    <div class=\"prod_main\">\n      <ion-label class=\"title\">Tamaño</ion-label>\n\n      <div class=\"flex_div\">\n        <div class=\"inner_div\" (click)=\"changeSize(item)\" [class.active]=\"size == item\"\n          *ngFor=\"let item of [6,7,8,9,10,11]\">\n          <ion-label>{{item}}</ion-label>\n        </div>\n      </div>\n    </div>\n\n    <!-- <div class=\"size\">\n      <ion-label class=\"title\">Size</ion-label>\n      <ion-row>\n        <ion-col size=\"2\" *ngFor=\"let item of [6,7,8,9,10,11]\" text-center>\n          <span (click)=\"size = item\" [ngClass]=\"item ==size ? 'active':'normal'\">{{item}}</span>\n        </ion-col>\n      </ion-row>\n    </div> -->\n\n    <div class=\"prod_main\">\n      <ion-label class=\"title\">Detalle del producto</ion-label>\n      <ion-label class=\"sub_t\" style=\"font-weight: 600;margin-top: 10px !important;margin-bottom: 10px !important;\">What\n        it is</ion-label>\n\n      <ion-label class=\"dummy\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt\n        ut\n        labore et dolore magna\n        aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\n        consequat.</ion-label>\n      <ion-label class=\"dummy\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt\n        ut\n        labore et\n        dolore magna\n        aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\n        consequat.\n      </ion-label>\n      <ion-label class=\"dummy\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt\n        ut\n        labore et\n        dolore magna\n        aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\n        consequat.\n      </ion-label>\n    </div>\n\n\n    <div class=\"prod_main\">\n      <ion-label class=\"title\">Productos relacionados</ion-label>\n    </div>\n\n    <div>\n      <!-- <ion-label class=\"title\">Related Products</ion-label> -->\n      <ion-slides mode=\"ios\" [options]=\"slidesOptsProduct1\" margin-bottom margin-top>\n        <ion-slide *ngFor=\"let item of productSliderOne;let i = index\" (click)=\"onCategorySelect(item)\">\n          <ion-grid fixed>\n            <ion-row class=\"product_section\">\n              <ion-col size=\"6\">\n                <div class=\"product_div\" [style.backgroundImage]=\"'url( '+ item.img +' )'\"></div>\n              </ion-col>\n              <ion-col size=\"6\">\n                <div class=\"info\">\n                  <span class=\"line\"></span>\n                  <ion-label class=\"header\">{{item.header}}</ion-label>\n                  <ion-label class=\"desc\">{{item.desc}}</ion-label>\n                  <ion-label class=\"explore_btn\">+ Explora</ion-label>\n                  <span class=\"line\"></span>\n                </div>\n              </ion-col>\n            </ion-row>\n          </ion-grid>\n        </ion-slide>\n      </ion-slides>\n    </div>\n  </div>\n</ion-content>\n\n<ion-footer>\n  <ion-grid fixed style=\"padding-bottom: 10px !important;\">\n    <ion-row>\n      <ion-col size=\"6\" (click)=\"wishlist()\">\n        <ion-button fill=\"clear\">\n          <ion-icon name=\"bookmark\"></ion-icon> Guardar\n        </ion-button>\n      </ion-col>\n      <ion-col size=\"6\" (click)=\"cart()\">\n        <ion-button fill=\"clear\">\n          <ion-icon name=\"cart\"></ion-icon> Añadir al carrito\n        </ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-footer>";
    /***/
  },

  /***/
  "./src/app/pages/product/product.module.ts":
  /*!*************************************************!*\
    !*** ./src/app/pages/product/product.module.ts ***!
    \*************************************************/

  /*! exports provided: ProductPageModule */

  /***/
  function srcAppPagesProductProductModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ProductPageModule", function () {
      return ProductPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _product_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./product.page */
    "./src/app/pages/product/product.page.ts");
    /* harmony import */


    var _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @fortawesome/angular-fontawesome */
    "./node_modules/@fortawesome/angular-fontawesome/fesm2015/angular-fontawesome.js");

    var routes = [{
      path: '',
      component: _product_page__WEBPACK_IMPORTED_MODULE_6__["ProductPage"]
    }];

    var ProductPageModule = function ProductPageModule() {
      _classCallCheck(this, ProductPageModule);
    };

    ProductPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_7__["FontAwesomeModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_product_page__WEBPACK_IMPORTED_MODULE_6__["ProductPage"]]
    })], ProductPageModule);
    /***/
  },

  /***/
  "./src/app/pages/product/product.page.scss":
  /*!*************************************************!*\
    !*** ./src/app/pages/product/product.page.scss ***!
    \*************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesProductProductPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".div_img_back {\n  height: 40vh;\n  width: 100%;\n  background-position: center;\n  background-size: contain;\n  background-repeat: no-repeat;\n}\n\nion-label {\n  display: block;\n}\n\n.details .prod_main {\n  box-shadow: 9px 3px 16px 9px rgba(126, 126, 171, 0.2);\n  padding: 15px;\n  margin-bottom: 20px;\n}\n\n.details .prod_main .title {\n  font-weight: bolder;\n  font-size: 20px;\n  padding: 0px;\n  color: black;\n  margin: 0px !important;\n}\n\n.details .prod_main .sub_t {\n  font-size: 15px;\n  color: black;\n  margin: 0px !important;\n}\n\n.details .prod_main .feature {\n  font-size: 15px;\n  color: gray;\n  margin: 0px !important;\n}\n\n.details .prod_main .prod_orgin {\n  font-size: 15px;\n  margin: 0px;\n  text-decoration: line-through;\n  color: red;\n  display: inline !important;\n}\n\n.details .prod_main .prod_sell {\n  font-size: 15px;\n  margin: 0px;\n  color: darkolivegreen;\n  font-weight: bold;\n}\n\n.details .prod_main .dummy {\n  font-size: 12px;\n  font-weight: bolder;\n  color: gray;\n  padding: 0px 10px;\n  margin-bottom: 10px;\n}\n\n.details .prod_main .flex_div {\n  display: -webkit-box;\n  display: flex;\n  justify-content: space-around;\n}\n\n.details .prod_main .flex_div .inner_div {\n  height: 50px;\n  width: 50px;\n  border-radius: 50%;\n  border: 2px solid gray;\n  position: relative;\n}\n\n.details .prod_main .flex_div .inner_div ion-label {\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  -webkit-transform: translate(-50%, -50%);\n          transform: translate(-50%, -50%);\n  font-size: 20px;\n  font-weight: 600;\n  color: gray;\n}\n\n.details .prod_main .flex_div .active {\n  border-radius: 50%;\n  border: 2px solid #3b6098 !important;\n  background: #3b6098;\n}\n\n.details .prod_main .flex_div .active ion-label {\n  color: white !important;\n}\n\n.details .product_section {\n  box-shadow: 0 0 7px rgba(0, 0, 0, 0.3);\n  border: 1px solid lightgray;\n}\n\n.details .product_section .product_div {\n  width: 100%;\n  height: 100%;\n  background-position: center;\n  background-size: cover;\n  background-repeat: no-repeat;\n}\n\n.details .product_section .info {\n  margin-left: 10px;\n}\n\n.details .product_section .info ion-label {\n  margin-bottom: 10px;\n}\n\n.details .product_section .info .line {\n  width: 20%;\n  margin-top: 10px;\n  margin-bottom: 10px;\n  height: 2px;\n  background-color: gray;\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: left;\n          justify-content: left;\n}\n\n.details .product_section .info .header {\n  font-size: 10px;\n  text-align: left;\n  color: gray;\n}\n\n.details .product_section .info .desc {\n  font-size: 12px;\n  text-align: left;\n  font-weight: bold;\n}\n\n.details .product_section .info .explore_btn {\n  font-size: 10px;\n  text-align: left;\n  color: gray;\n}\n\nion-footer ion-col {\n  text-align: center;\n}\n\nion-footer ion-col ion-button {\n  font-size: 15px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvcHJvZHVjdC9DOlxcVXNlcnNcXFVzZXJcXERvd25sb2Fkc1xcaW9uaWM1U2hvcEFwcC01eWxwZGNcXGlvbmljNVNob3BBcHBcXEFwcF9jb2RlL3NyY1xcYXBwXFxwYWdlc1xccHJvZHVjdFxccHJvZHVjdC5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL3Byb2R1Y3QvcHJvZHVjdC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLDJCQUFBO0VBQ0Esd0JBQUE7RUFDQSw0QkFBQTtBQ0NKOztBRENBO0VBQ0ksY0FBQTtBQ0VKOztBREdJO0VBQ0kscURBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7QUNBUjs7QURFUTtFQUNJLG1CQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0Esc0JBQUE7QUNBWjs7QURFUTtFQUNJLGVBQUE7RUFDQSxZQUFBO0VBQ0Esc0JBQUE7QUNBWjs7QURFUTtFQUNJLGVBQUE7RUFDQSxXQUFBO0VBQ0Esc0JBQUE7QUNBWjs7QURFUTtFQUNJLGVBQUE7RUFDQSxXQUFBO0VBQ0EsNkJBQUE7RUFDQSxVQUFBO0VBQ0EsMEJBQUE7QUNBWjs7QURFUTtFQUNJLGVBQUE7RUFDQSxXQUFBO0VBQ0EscUJBQUE7RUFDQSxpQkFBQTtBQ0FaOztBREVRO0VBQ0ksZUFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7QUNBWjs7QURHUTtFQUNJLG9CQUFBO0VBQUEsYUFBQTtFQUNBLDZCQUFBO0FDRFo7O0FER1k7RUFDSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0Esc0JBQUE7RUFDQSxrQkFBQTtBQ0RoQjs7QURFZ0I7RUFDSSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0VBQ0Esd0NBQUE7VUFBQSxnQ0FBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7QUNBcEI7O0FER1k7RUFDSSxrQkFBQTtFQUNBLG9DQUFBO0VBQ0EsbUJBQUE7QUNEaEI7O0FER2dCO0VBQ0ksdUJBQUE7QUNEcEI7O0FET0k7RUFDSSxzQ0FBQTtFQUNBLDJCQUFBO0FDTFI7O0FET1E7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLDJCQUFBO0VBQ0Esc0JBQUE7RUFDQSw0QkFBQTtBQ0xaOztBRE9RO0VBQ0ksaUJBQUE7QUNMWjs7QURPWTtFQUNJLG1CQUFBO0FDTGhCOztBRE9ZO0VBQ1EsVUFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0VBQ0Esc0JBQUE7RUFDQSxvQkFBQTtFQUFBLGFBQUE7RUFDQSxzQkFBQTtVQUFBLHFCQUFBO0FDTHBCOztBRE9nQjtFQUNJLGVBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7QUNMcEI7O0FET2dCO0VBQ0ksZUFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7QUNMcEI7O0FET2dCO0VBQ0ksZUFBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtBQ0xwQjs7QURZSTtFQUNJLGtCQUFBO0FDVFI7O0FEV1E7RUFDSSxlQUFBO0FDVFoiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9wcm9kdWN0L3Byb2R1Y3QucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmRpdl9pbWdfYmFja3tcbiAgICBoZWlnaHQ6IDQwdmg7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xufVxuaW9uLWxhYmVsIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbn1cblxuLmRldGFpbHN7XG4gXG4gICAgLnByb2RfbWFpbntcbiAgICAgICAgYm94LXNoYWRvdzogOXB4IDNweCAxNnB4IDlweCByZ2JhKDEyNiwgMTI2LCAxNzEsIDAuMik7XG4gICAgICAgIHBhZGRpbmc6IDE1cHg7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XG5cbiAgICAgICAgLnRpdGxle1xuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweDtcbiAgICAgICAgICAgIGNvbG9yOiBibGFjaztcbiAgICAgICAgICAgIG1hcmdpbjogMHB4ICFpbXBvcnRhbnQ7XG4gICAgICAgIH1cbiAgICAgICAgLnN1Yl90e1xuICAgICAgICAgICAgZm9udC1zaXplOiAxNXB4O1xuICAgICAgICAgICAgY29sb3I6IGJsYWNrO1xuICAgICAgICAgICAgbWFyZ2luOiAwcHggIWltcG9ydGFudDtcbiAgICAgICAgfVxuICAgICAgICAuZmVhdHVyZXtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgICAgICAgICAgIGNvbG9yOiBncmF5O1xuICAgICAgICAgICAgbWFyZ2luOiAwcHggIWltcG9ydGFudDtcbiAgICAgICAgfVxuICAgICAgICAucHJvZF9vcmdpbntcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTVweDsgXG4gICAgICAgICAgICBtYXJnaW46IDBweDtcbiAgICAgICAgICAgIHRleHQtZGVjb3JhdGlvbjpsaW5lLXRocm91Z2g7XG4gICAgICAgICAgICBjb2xvcjogcmVkO1xuICAgICAgICAgICAgZGlzcGxheTogaW5saW5lICFpbXBvcnRhbnQ7XG4gICAgICAgIH1cbiAgICAgICAgLnByb2Rfc2VsbHtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTVweDsgXG4gICAgICAgICAgICBtYXJnaW46IDBweDsgICBcbiAgICAgICAgICAgIGNvbG9yOiBkYXJrb2xpdmVncmVlbjtcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgICB9XG4gICAgICAgIC5kdW1teXtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG4gICAgICAgICAgICBjb2xvcjogZ3JheTtcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweCAxMHB4O1xuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMTBweDsgICBcbiAgICAgICAgfVxuXG4gICAgICAgIC5mbGV4X2RpdntcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcblxuICAgICAgICAgICAgLmlubmVyX2RpdntcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDUwcHg7XG4gICAgICAgICAgICAgICAgd2lkdGg6IDUwcHg7XG4gICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgICAgICAgICAgICAgIGJvcmRlcjogMnB4IHNvbGlkIGdyYXk7XG4gICAgICAgICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgICAgICAgICAgIGlvbi1sYWJlbCB7XG4gICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICAgICAgICAgICAgdG9wOiA1MCU7XG4gICAgICAgICAgICAgICAgICAgIGxlZnQ6IDUwJTtcbiAgICAgICAgICAgICAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwtNTAlKTtcbiAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAyMHB4O1xuICAgICAgICAgICAgICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgICAgICAgICAgICAgICAgICBjb2xvcjogZ3JheTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAuYWN0aXZle1xuICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICAgICAgICAgICAgICBib3JkZXI6IDJweCBzb2xpZCAjM2I2MDk4ICFpbXBvcnRhbnQ7XG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZDogIzNiNjA5ODtcblxuICAgICAgICAgICAgICAgIGlvbi1sYWJlbCB7XG4gICAgICAgICAgICAgICAgICAgIGNvbG9yOiB3aGl0ZSAhaW1wb3J0YW50O1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxuICAgIC5wcm9kdWN0X3NlY3Rpb257XG4gICAgICAgIGJveC1zaGFkb3c6IDAgMCA3cHggcmdiYSgwLCAwLCAwLCAwLjMpO1xuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCBsaWdodGdyYXk7XG4gICAgICAgIFxuICAgICAgICAucHJvZHVjdF9kaXZ7XG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgICAgIGhlaWdodDogMTAwJTtcbiAgICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgICAgICAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgICAgICB9XG4gICAgICAgIC5pbmZve1xuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG5cbiAgICAgICAgICAgIGlvbi1sYWJlbCB7XG4gICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC5saW5le1xuICAgICAgICAgICAgICAgICAgICB3aWR0aDogMjAlO1xuICAgICAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xuICAgICAgICAgICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6MnB4O1xuICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiBncmF5O1xuICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGxlZnQ7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIC5oZWFkZXJ7XG4gICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTBweDtcbiAgICAgICAgICAgICAgICAgICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICAgICAgICAgICAgICAgICAgY29sb3I6IGdyYXk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIC5kZXNje1xuICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDEycHg7XG4gICAgICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAuZXhwbG9yZV9idG57XG4gICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTBweDtcbiAgICAgICAgICAgICAgICAgICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICAgICAgICAgICAgICAgICAgY29sb3I6IGdyYXk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxufVxuXG5pb24tZm9vdGVye1xuICAgIGlvbi1jb2x7XG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcblxuICAgICAgICBpb24tYnV0dG9ue1xuICAgICAgICAgICAgZm9udC1zaXplOiAxNXB4O1xuICAgICAgICB9XG4gICAgfVxufSIsIi5kaXZfaW1nX2JhY2sge1xuICBoZWlnaHQ6IDQwdmg7XG4gIHdpZHRoOiAxMDAlO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbn1cblxuaW9uLWxhYmVsIHtcbiAgZGlzcGxheTogYmxvY2s7XG59XG5cbi5kZXRhaWxzIC5wcm9kX21haW4ge1xuICBib3gtc2hhZG93OiA5cHggM3B4IDE2cHggOXB4IHJnYmEoMTI2LCAxMjYsIDE3MSwgMC4yKTtcbiAgcGFkZGluZzogMTVweDtcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcbn1cbi5kZXRhaWxzIC5wcm9kX21haW4gLnRpdGxlIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBwYWRkaW5nOiAwcHg7XG4gIGNvbG9yOiBibGFjaztcbiAgbWFyZ2luOiAwcHggIWltcG9ydGFudDtcbn1cbi5kZXRhaWxzIC5wcm9kX21haW4gLnN1Yl90IHtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBjb2xvcjogYmxhY2s7XG4gIG1hcmdpbjogMHB4ICFpbXBvcnRhbnQ7XG59XG4uZGV0YWlscyAucHJvZF9tYWluIC5mZWF0dXJlIHtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBjb2xvcjogZ3JheTtcbiAgbWFyZ2luOiAwcHggIWltcG9ydGFudDtcbn1cbi5kZXRhaWxzIC5wcm9kX21haW4gLnByb2Rfb3JnaW4ge1xuICBmb250LXNpemU6IDE1cHg7XG4gIG1hcmdpbjogMHB4O1xuICB0ZXh0LWRlY29yYXRpb246IGxpbmUtdGhyb3VnaDtcbiAgY29sb3I6IHJlZDtcbiAgZGlzcGxheTogaW5saW5lICFpbXBvcnRhbnQ7XG59XG4uZGV0YWlscyAucHJvZF9tYWluIC5wcm9kX3NlbGwge1xuICBmb250LXNpemU6IDE1cHg7XG4gIG1hcmdpbjogMHB4O1xuICBjb2xvcjogZGFya29saXZlZ3JlZW47XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmRldGFpbHMgLnByb2RfbWFpbiAuZHVtbXkge1xuICBmb250LXNpemU6IDEycHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG4gIGNvbG9yOiBncmF5O1xuICBwYWRkaW5nOiAwcHggMTBweDtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbn1cbi5kZXRhaWxzIC5wcm9kX21haW4gLmZsZXhfZGl2IHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG59XG4uZGV0YWlscyAucHJvZF9tYWluIC5mbGV4X2RpdiAuaW5uZXJfZGl2IHtcbiAgaGVpZ2h0OiA1MHB4O1xuICB3aWR0aDogNTBweDtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBib3JkZXI6IDJweCBzb2xpZCBncmF5O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4uZGV0YWlscyAucHJvZF9tYWluIC5mbGV4X2RpdiAuaW5uZXJfZGl2IGlvbi1sYWJlbCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA1MCU7XG4gIGxlZnQ6IDUwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgY29sb3I6IGdyYXk7XG59XG4uZGV0YWlscyAucHJvZF9tYWluIC5mbGV4X2RpdiAuYWN0aXZlIHtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBib3JkZXI6IDJweCBzb2xpZCAjM2I2MDk4ICFpbXBvcnRhbnQ7XG4gIGJhY2tncm91bmQ6ICMzYjYwOTg7XG59XG4uZGV0YWlscyAucHJvZF9tYWluIC5mbGV4X2RpdiAuYWN0aXZlIGlvbi1sYWJlbCB7XG4gIGNvbG9yOiB3aGl0ZSAhaW1wb3J0YW50O1xufVxuLmRldGFpbHMgLnByb2R1Y3Rfc2VjdGlvbiB7XG4gIGJveC1zaGFkb3c6IDAgMCA3cHggcmdiYSgwLCAwLCAwLCAwLjMpO1xuICBib3JkZXI6IDFweCBzb2xpZCBsaWdodGdyYXk7XG59XG4uZGV0YWlscyAucHJvZHVjdF9zZWN0aW9uIC5wcm9kdWN0X2RpdiB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbn1cbi5kZXRhaWxzIC5wcm9kdWN0X3NlY3Rpb24gLmluZm8ge1xuICBtYXJnaW4tbGVmdDogMTBweDtcbn1cbi5kZXRhaWxzIC5wcm9kdWN0X3NlY3Rpb24gLmluZm8gaW9uLWxhYmVsIHtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbn1cbi5kZXRhaWxzIC5wcm9kdWN0X3NlY3Rpb24gLmluZm8gLmxpbmUge1xuICB3aWR0aDogMjAlO1xuICBtYXJnaW4tdG9wOiAxMHB4O1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICBoZWlnaHQ6IDJweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogZ3JheTtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBsZWZ0O1xufVxuLmRldGFpbHMgLnByb2R1Y3Rfc2VjdGlvbiAuaW5mbyAuaGVhZGVyIHtcbiAgZm9udC1zaXplOiAxMHB4O1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBjb2xvcjogZ3JheTtcbn1cbi5kZXRhaWxzIC5wcm9kdWN0X3NlY3Rpb24gLmluZm8gLmRlc2Mge1xuICBmb250LXNpemU6IDEycHg7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmRldGFpbHMgLnByb2R1Y3Rfc2VjdGlvbiAuaW5mbyAuZXhwbG9yZV9idG4ge1xuICBmb250LXNpemU6IDEwcHg7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIGNvbG9yOiBncmF5O1xufVxuXG5pb24tZm9vdGVyIGlvbi1jb2wge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5pb24tZm9vdGVyIGlvbi1jb2wgaW9uLWJ1dHRvbiB7XG4gIGZvbnQtc2l6ZTogMTVweDtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/pages/product/product.page.ts":
  /*!***********************************************!*\
    !*** ./src/app/pages/product/product.page.ts ***!
    \***********************************************/

  /*! exports provided: ProductPage */

  /***/
  function srcAppPagesProductProductPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ProductPage", function () {
      return ProductPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/app/services/dummy-data.service */
    "./src/app/services/dummy-data.service.ts");
    /*
    
      Authors : initappz (Rahul Jograna)
      Website : https://initappz.com/
      Created : 17-March-2020
      This App Template Source code is licensed as per the
      terms found in the Website https://initappz.com/license
      Copyright and Good Faith Purchasers © 2020-present initappz.
    
    */


    var ProductPage =
    /*#__PURE__*/
    function () {
      function ProductPage(toastController, router, dummy) {
        _classCallCheck(this, ProductPage);

        this.toastController = toastController;
        this.router = router;
        this.dummy = dummy;
        this.slidesOptsProduct1 = {
          slidesPerView: 1.3
        };
        this.productSliderOne = [];
        this.size = 6;
        this.images = ['assets/products/1.jpg', 'assets/products/4.jpg', 'assets/products/5.jpeg', 'assets/products/6.jpg'];
        this.productSliderOne = this.dummy.productSlider;
      }

      _createClass(ProductPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "wishlist",
        value: function wishlist() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee() {
            var toast;
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    _context.next = 2;
                    return this.toastController.create({
                      message: 'Added to wishlist',
                      duration: 2000
                    });

                  case 2:
                    toast = _context.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "cart",
        value: function cart() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee2() {
            var toast;
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    _context2.next = 2;
                    return this.toastController.create({
                      message: 'Added to cart',
                      duration: 2000
                    });

                  case 2:
                    toast = _context2.sent;
                    toast.present();
                    this.router.navigate(['/cart']);

                  case 5:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "onCart",
        value: function onCart() {}
      }, {
        key: "onCategorySelect",
        value: function onCategorySelect(item) {
          this.router.navigate(['']);
        }
      }, {
        key: "changeSize",
        value: function changeSize(val) {
          this.size = val;
        }
      }]);

      return ProductPage;
    }();

    ProductPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
      }, {
        type: src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_4__["DummyDataService"]
      }];
    };

    ProductPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-product',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./product.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/product/product.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./product.page.scss */
      "./src/app/pages/product/product.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_4__["DummyDataService"]])], ProductPage);
    /***/
  }
}]);
//# sourceMappingURL=pages-product-product-module-es5.js.map