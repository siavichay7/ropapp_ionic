(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-account-orders-orders-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/account/orders/orders.page.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/account/orders/orders.page.html ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>Orders</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"bg-light\">\n\n  <div class=\"prod_main\">\n    <ion-label class=\"title\">Upcomming Orders</ion-label>\n  </div>\n\n  <ion-card style=\"position: relative;\" (click)=\"goToOrderDetail()\">\n    <ion-card-header>\n      <h4>Product 1</h4>\n      <ion-label class=\"price\">300$</ion-label>\n      <ion-label class=\"order_lbl\">2513254112</ion-label>\n      <ion-label>Placed on <span>28/12/2017</span></ion-label>\n      <ion-label class=\"gray_lbl\">Deliverd On</ion-label>\n      <ion-label class=\"gray_lbl\">Wed, March 21-03-2018</ion-label>  \n    </ion-card-header>\n  </ion-card>\n\n  <div class=\"prod_main\">\n    <ion-label class=\"title\">Old Orders</ion-label>\n  </div>\n\n  <ion-card style=\"position: relative;\" (click)=\"goToOrderDetail()\" *ngFor=\"let ites of [1,2,3]\"> \n    <ion-card-header>\n      <h4>Product 1</h4>\n      <ion-label class=\"price\">300$</ion-label>\n      <ion-label class=\"order_lbl\">2513254112</ion-label>\n      <ion-label>Placed on <span>28/12/2016</span></ion-label>\n      <ion-label class=\"gray_lbl\">Deliverd On</ion-label>\n      <ion-label class=\"gray_lbl\">Wed, March 21-03-2018</ion-label>  \n    </ion-card-header>\n  </ion-card>\n\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/account/orders/orders.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/account/orders/orders.module.ts ***!
  \*******************************************************/
/*! exports provided: OrdersPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrdersPageModule", function() { return OrdersPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _orders_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./orders.page */ "./src/app/pages/account/orders/orders.page.ts");







const routes = [
    {
        path: '',
        component: _orders_page__WEBPACK_IMPORTED_MODULE_6__["OrdersPage"]
    }
];
let OrdersPageModule = class OrdersPageModule {
};
OrdersPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_orders_page__WEBPACK_IMPORTED_MODULE_6__["OrdersPage"]]
    })
], OrdersPageModule);



/***/ }),

/***/ "./src/app/pages/account/orders/orders.page.scss":
/*!*******************************************************!*\
  !*** ./src/app/pages/account/orders/orders.page.scss ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-label {\n  display: block;\n}\n\n.prod_main {\n  box-shadow: 9px 3px 16px 9px rgba(126, 126, 171, 0.2);\n  padding: 15px;\n  margin-bottom: 20px;\n  margin-top: 20px;\n}\n\n.prod_main .title {\n  font-weight: bolder;\n  font-size: 20px;\n  padding: 0px;\n  color: black;\n  margin: 0px !important;\n}\n\nion-card-header {\n  padding: 10px;\n  position: relative;\n}\n\nion-card-header ion-label {\n  margin-bottom: 10px;\n}\n\nion-card-header span {\n  color: #999 !important;\n  font-weight: 600;\n}\n\nion-card-header .price {\n  font-weight: 600;\n}\n\nion-card-header .gray_lbl {\n  color: #999;\n}\n\nion-card-header .order_lbl {\n  position: absolute;\n  top: 0;\n  right: 0;\n  padding: 8px;\n  background: var(--ion-color-primary);\n  color: white;\n  font-size: 16px;\n  font-weight: bold;\n}\n\nion-card-content {\n  padding: 10px;\n}\n\nion-card-content .first_col {\n  padding: 0px !important;\n}\n\nion-card-content ion-label {\n  margin-bottom: 10px;\n}\n\nion-card-content .gray_lbl {\n  color: #999;\n}\n\nion-card-content .price {\n  font-weight: 600;\n}\n\nion-card-content ion-button {\n  --border-radius: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYWNjb3VudC9vcmRlcnMvQzpcXFVzZXJzXFxVc2VyXFxEb3dubG9hZHNcXGlvbmljNVNob3BBcHAtNXlscGRjXFxpb25pYzVTaG9wQXBwXFxBcHBfY29kZS9zcmNcXGFwcFxccGFnZXNcXGFjY291bnRcXG9yZGVyc1xcb3JkZXJzLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvYWNjb3VudC9vcmRlcnMvb3JkZXJzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGNBQUE7QUNDSjs7QURDQTtFQUNJLHFEQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7QUNFSjs7QURBSTtFQUNJLG1CQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0Esc0JBQUE7QUNFUjs7QURFQTtFQUNJLGFBQUE7RUFDQSxrQkFBQTtBQ0NKOztBREFJO0VBQ0ksbUJBQUE7QUNFUjs7QURBSTtFQUNJLHNCQUFBO0VBQ0EsZ0JBQUE7QUNFUjs7QURBSTtFQUNJLGdCQUFBO0FDRVI7O0FEQUk7RUFDSSxXQUFBO0FDRVI7O0FEQUk7RUFDSSxrQkFBQTtFQUNBLE1BQUE7RUFDQSxRQUFBO0VBQ0EsWUFBQTtFQUNBLG9DQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQ0VSOztBREVBO0VBQ0ksYUFBQTtBQ0NKOztBRENJO0VBQ0ksdUJBQUE7QUNDUjs7QURFSTtFQUNJLG1CQUFBO0FDQVI7O0FER0k7RUFDSSxXQUFBO0FDRFI7O0FER0k7RUFDSSxnQkFBQTtBQ0RSOztBREdJO0VBQ0ksb0JBQUE7QUNEUiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2FjY291bnQvb3JkZXJzL29yZGVycy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tbGFiZWx7XG4gICAgZGlzcGxheTogYmxvY2s7XG59XG4ucHJvZF9tYWlue1xuICAgIGJveC1zaGFkb3c6IDlweCAzcHggMTZweCA5cHggcmdiYSgxMjYsIDEyNiwgMTcxLCAwLjIpO1xuICAgIHBhZGRpbmc6IDE1cHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMjBweDtcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xuXG4gICAgLnRpdGxle1xuICAgICAgICBmb250LXdlaWdodDogYm9sZGVyO1xuICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgIHBhZGRpbmc6IDBweDtcbiAgICAgICAgY29sb3I6IGJsYWNrO1xuICAgICAgICBtYXJnaW46IDBweCAhaW1wb3J0YW50O1xuICAgIH1cbn1cblxuaW9uLWNhcmQtaGVhZGVye1xuICAgIHBhZGRpbmc6IDEwcHg7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGlvbi1sYWJlbCB7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gICAgfVxuICAgIHNwYW4ge1xuICAgICAgICBjb2xvcjogIzk5OSAhaW1wb3J0YW50OyAgICAgICAgXG4gICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgfVxuICAgIC5wcmljZXtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICB9XG4gICAgLmdyYXlfbGJse1xuICAgICAgICBjb2xvcjogIzk5OTtcbiAgICB9XG4gICAgLm9yZGVyX2xibHtcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICB0b3A6IDA7XG4gICAgICAgIHJpZ2h0OiAwO1xuICAgICAgICBwYWRkaW5nOiA4cHg7XG4gICAgICAgIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbiAgICAgICAgY29sb3I6IHdoaXRlO1xuICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIH1cbn1cblxuaW9uLWNhcmQtY29udGVudHtcbiAgICBwYWRkaW5nOiAxMHB4O1xuXG4gICAgLmZpcnN0X2NvbHtcbiAgICAgICAgcGFkZGluZzogMHB4ICFpbXBvcnRhbnQ7XG4gICAgfVxuICAgIFxuICAgIGlvbi1sYWJlbCB7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gICAgfVxuXG4gICAgLmdyYXlfbGJse1xuICAgICAgICBjb2xvcjogIzk5OTtcbiAgICB9XG4gICAgLnByaWNle1xuICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgIH1cbiAgICBpb24tYnV0dG9ue1xuICAgICAgICAtLWJvcmRlci1yYWRpdXM6IDVweDtcbiAgICB9XG59IiwiaW9uLWxhYmVsIHtcbiAgZGlzcGxheTogYmxvY2s7XG59XG5cbi5wcm9kX21haW4ge1xuICBib3gtc2hhZG93OiA5cHggM3B4IDE2cHggOXB4IHJnYmEoMTI2LCAxMjYsIDE3MSwgMC4yKTtcbiAgcGFkZGluZzogMTVweDtcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcbiAgbWFyZ2luLXRvcDogMjBweDtcbn1cbi5wcm9kX21haW4gLnRpdGxlIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBwYWRkaW5nOiAwcHg7XG4gIGNvbG9yOiBibGFjaztcbiAgbWFyZ2luOiAwcHggIWltcG9ydGFudDtcbn1cblxuaW9uLWNhcmQtaGVhZGVyIHtcbiAgcGFkZGluZzogMTBweDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuaW9uLWNhcmQtaGVhZGVyIGlvbi1sYWJlbCB7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG59XG5pb24tY2FyZC1oZWFkZXIgc3BhbiB7XG4gIGNvbG9yOiAjOTk5ICFpbXBvcnRhbnQ7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG59XG5pb24tY2FyZC1oZWFkZXIgLnByaWNlIHtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cbmlvbi1jYXJkLWhlYWRlciAuZ3JheV9sYmwge1xuICBjb2xvcjogIzk5OTtcbn1cbmlvbi1jYXJkLWhlYWRlciAub3JkZXJfbGJsIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDA7XG4gIHJpZ2h0OiAwO1xuICBwYWRkaW5nOiA4cHg7XG4gIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDE2cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG5pb24tY2FyZC1jb250ZW50IHtcbiAgcGFkZGluZzogMTBweDtcbn1cbmlvbi1jYXJkLWNvbnRlbnQgLmZpcnN0X2NvbCB7XG4gIHBhZGRpbmc6IDBweCAhaW1wb3J0YW50O1xufVxuaW9uLWNhcmQtY29udGVudCBpb24tbGFiZWwge1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xufVxuaW9uLWNhcmQtY29udGVudCAuZ3JheV9sYmwge1xuICBjb2xvcjogIzk5OTtcbn1cbmlvbi1jYXJkLWNvbnRlbnQgLnByaWNlIHtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cbmlvbi1jYXJkLWNvbnRlbnQgaW9uLWJ1dHRvbiB7XG4gIC0tYm9yZGVyLXJhZGl1czogNXB4O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/account/orders/orders.page.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/account/orders/orders.page.ts ***!
  \*****************************************************/
/*! exports provided: OrdersPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrdersPage", function() { return OrdersPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");

/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/


let OrdersPage = class OrdersPage {
    constructor(router) {
        this.router = router;
    }
    ngOnInit() {
    }
    onClick() {
    }
    goToOrderDetail() {
        this.router.navigate(['/order-details']);
    }
};
OrdersPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
OrdersPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-orders',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./orders.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/account/orders/orders.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./orders.page.scss */ "./src/app/pages/account/orders/orders.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
], OrdersPage);



/***/ })

}]);
//# sourceMappingURL=pages-account-orders-orders-module-es2015.js.map