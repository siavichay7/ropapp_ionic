import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DummyDataService {

  products: any[] = [
    {
      img: 'https://i.pinimg.com/originals/32/b9/2d/32b92d0b5c648689e36a6fd0a40c0bbf.jpg',
      off: 'Min. 60% off.',
      name : 'Product 1',
      price : '25.00',
      sale : '20.00'
    },
    {
      img: 'https://i.pinimg.com/originals/d4/11/a2/d411a2d3d539a6934eb328c1b2d3c971.jpg',
      off: 'Min. 50% off.',
      name : 'Product 2',
      price : '23.00',
      sale : '200.00'
    },
    {
      img: 'https://static.bershka.net/4/photos2//2018/V/0/2/p/0264/251/400/0264251400_1_1_3.jpg?t=1514970277706',
      off: 'Min. 50% off.',
      name : 'Product 3',
      price : '22.00',
      sale : '17.00'
    },
    {
      img: 'https://rebajas2017.com/wp-content/uploads/2016/09/rebajas-bershka-2017-moda-hombre-sudaderas.jpg',
      off: 'Min. 40% off.',
      name : 'Product 4',
      price : '15.00',
      sale : '13.00'
    },
    {
      img: 'assets/products/6.jpg',
      off: 'Min. 45% off.',
      name : 'Product 5',
      price : '17.00',
      sale : '14.00'
    },
    {
      img: 'assets/products/7.jpg',
      off: 'Min. 70% off.',
      name : 'Product 6',
      price : '20.00',
      sale : '15.00'
    },
    {
      img: 'assets/products/8.jpeg',
      off: 'Min. 30% off.',
      name : 'Product 7',
      price : '56.00',
      sale : '50.00'
    },
    {
      img: 'assets/products/9.jpeg',
      off: 'Min. 50% off.',
      name : 'Product 8',
      price : '46.00',
      sale : '35.00'
    },
    {
      img: 'assets/products/10.jpg',
      off: 'Min. 60% off.',
      name : 'Product 9',
      price : '34.00',
      sale : '30.00'
    },
    {
      img: 'assets/products/1.jpg',
      off: 'Min. 60% off.',
      name : 'Product 10',
      price : '20.00',
      sale : '15.00'
    },
    {
      img: 'assets/products/2.jpg',
      off: 'Min. 50% off.',
      name : 'Product 11',
      price : '34.00',
      sale : '23.00'
    },
    {
      img: 'assets/products/3.jpg',
      off: 'Min. 40% off.',
      name : 'Product 12',
      price : '56.00',
      sale : '50.00'
    },
    {
      img: 'assets/products/4.jpg',
      off: 'Min. 50% off.',
      name : 'Product 13',
      price : '30.00',
      sale : '25.00'
    },
    {
      img: 'assets/products/5.jpeg',
      off: 'Min. 40% off.',
      name : 'Product 14',
      price : '60.00',
      sale : '45.00'
    },
    {
      img: 'assets/products/6.jpg',
      off: 'Min. 45% off.',
      name : 'Product 15',
      price : '50.00',
      sale : '45.00',
    },
    {
      img: 'assets/products/7.jpg',
      off: 'Min. 70% off.',
      name : 'Product 16',
      price : '30.00',
      sale : '24.00'
    },
    {
      img: 'assets/products/8.jpeg',
      off: 'Min. 30% off.',
      name : 'Product 17',
      price : '25.00',
      sale : '22.00'
    },
    {
      img: 'assets/products/9.jpeg',
      off: 'Min. 50% off.',
      name : 'Product 18',
      price : '22.00',
      sale : '17.00'
    },
    {
      img: 'assets/products/10.jpg',
      off: 'Min. 60% off.',
      name : 'Product 19',
      price : '28.00',
      sale : '25.00'
    },
    {
      img: 'assets/products/3.jpg',
      off: 'Min. 40% off.',
      name : 'Product 20',
      price : '20.00',
      sale : '15.00'
    },
  ];

  productSlider = [
    {
      img: 'assets/shop/7.jpg',
      header: 'HOT DEALS',
      desc: 'Flat $2 Off On Your Favorite Styles'
    },
    {
      img: 'assets/shop/12.jpg',
      header: 'HOT DEALS',
      desc: 'Extra 5% Off On your Order'
    },
    {
      img: 'assets/shop/24.jpg',
      header: 'HOT DEALS',
      desc: 'Flat $2 Off On Your Favorite Styles'
    },
    {
      img: 'assets/shop/22.jpg',
      header: 'HOT DEALS',
      desc: 'Flat $2 Off On Your Favorite Styles'
    },
    {
      img: 'assets/shop/6.jpg',
      header: 'HOT DEALS',
      desc: 'Flat $2 Off On Your Favorite Styles'
    },
  ];

  specialForYou = [
    {
      img: 'https://www.modaes.com/files//000_2016/nike/nike-interior-tienda-728.jpg',
      header: 'Nike',
      footer: '+ SHOP'
    },
    {
      img: 'https://www.palco23.com/files/2020/19_redaccion/equipamiento/adidas/adidas-outlet-728.jpg',
      header: 'Adidas',
      footer: '+ FOR HER'
    },
    {
      img: 'https://www.citymall.com.ec/wp-content/uploads/2018/04/puma-citymall2.jpg',
      header: 'Puma',
      footer: '+ SHOP'
    },
  ];

  playera = [
    {
      img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSFAyTFiv6GyzZH_eOOoxpDlzCGuxRnh28r3sVx0EZnUTKkxHKQ&usqp=CAU',
      header: 'Sumbawa',
      footer: '+ SHOP'
    },
    {
      img: 'https://www.modaes.com/files//000_2016/boardriders/quiksilver-tienda-728.jpg',
      header: 'Quicksilver',
      footer: '+ FOR HER'
    },
    {
      img: 'https://medias.fashionnetwork.com/image/upload/v1/medias/69873180a9af46671fa30e08658b75c62634747.jpg',
      header: 'DC',
      footer: '+ SHOP'
    },
  ];

  otro = [
    {
      img: 'https://medias.fashionnetwork.com/image/upload/v1/medias/eceb33b2090b9bee4c95409ac9d68b752976375.jpg',
      header: 'Levi',
      footer: '+ SHOP'
    },
    {
      img: 'https://medias.fashionnetwork.com/image/upload/v1/medias/70b413960ddecf039e3d7cad456f0ffb1481564.jpg',
      header: 'Lee',
      footer: '+ FOR HER'
    },
    {
      img: 'https://www.modaes.com/files/000_2016/sociedad%20textil%20lonia/Textil%20Lonia%20CH%20Carolina%20Herrera%20tienda%20exterior%20728.png',
      header: 'Carolina Herrera',
      footer: '+ SHOP'
    },
  ];

  catSlider = [
    {
      slug: 'men',
      name: 'MEN',
      img: 'assets/header/men.png'
    },
    {
      slug: 'women',
      name: 'WOMEN',
      img: 'assets/header/women.png'
    },
    {
      slug: 'sale',
      name: 'Sale',
      img: 'assets/header/sale.png'
    },
    {
      slug: 'kids',
      name: 'KIDS',
      img: 'assets/header/kids.png'
    },
    {
      slug: 'beatuty',
      name: 'BEATUTY',
      img: 'assets/header/beatuty.png'
    },
    {
      slug: 'home',
      name: 'HOME',
      img: 'assets/header/home.png'
    },
    {
      slug: 'gadgets',
      name: 'GADGETS',
      img: 'assets/header/gadgets.png'
    },
  ];
  constructor() { }
}
