(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-products-products-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/products/products.page.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/products/products.page.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button mode=\"md\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>\n      Adidas\n    </ion-title>\n    <ion-icon slot=\"end\" (click)=\"onSearch()\" class=\"header_icons\" name=\"search\"></ion-icon>\n    <ion-icon slot=\"end\" (click)=\"onBookmark()\" class=\"header_icons\" name=\"bookmark\"></ion-icon>\n    <ion-icon slot=\"end\" (click)=\"onCart()\" class=\"header_icons\" name=\"basket\"></ion-icon>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-grid fixed class=\"ion-no-margin ion-no-padding\">\n    <ion-row>\n      <ion-col size=\"6\" class=\"border_col\" *ngFor=\"let item of dummyData\" (click)=\"product()\">\n        <!-- <img src=\"assets/products/2.jpg\" alt=\"\" class=\"prod_img\"> -->\n        <div class=\"pro_image\" [style.backgroundImage]=\"'url(' + item.img + ')'\"></div>\n        <ion-grid fixed class=\"ion-no-padding ion-no-margin\">\n          <ion-row class=\"ion-no-padding ion-no-margin\">\n            <ion-col size=\"10\" class=\"ion-no-padding ion-no-margin\">\n              <p class=\"prod_title\">{{item.name}}</p>\n              <p class=\"prod_orgin\">${{item.price}} </p>&nbsp; <span class=\"prod_sell\">${{item.sale}}</span>\n            </ion-col>\n            <ion-col size=\"2\" class=\"ion-no-padding ion-no-margin\">\n              <ion-icon class=\"heart\" name=\"heart-outline\"></ion-icon>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n\n<ion-footer>\n  <ion-grid fixed style=\"padding-bottom: 10px !important;\">\n    <ion-row>\n      <ion-col size=\"6\" (click)=\"sorting()\">\n        <fa-icon [icon]=\"['fas', 'sort']\" style=\"color: gray !important;\"></fa-icon>\n        SORT\n      </ion-col>\n      <ion-col size=\"6\" (click)=\"filter()\">\n        <fa-icon [icon]=\"['fas', 'filter']\" style=\"color: gray !important;\"></fa-icon>\n        FILTER\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-footer>");

/***/ }),

/***/ "./src/app/pages/products/products.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/products/products.module.ts ***!
  \***************************************************/
/*! exports provided: ProductsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductsPageModule", function() { return ProductsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _products_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./products.page */ "./src/app/pages/products/products.page.ts");
/* harmony import */ var _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @fortawesome/angular-fontawesome */ "./node_modules/@fortawesome/angular-fontawesome/fesm2015/angular-fontawesome.js");








const routes = [
    {
        path: '',
        component: _products_page__WEBPACK_IMPORTED_MODULE_6__["ProductsPage"]
    }
];
let ProductsPageModule = class ProductsPageModule {
};
ProductsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_7__["FontAwesomeModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_products_page__WEBPACK_IMPORTED_MODULE_6__["ProductsPage"]]
    })
], ProductsPageModule);



/***/ }),

/***/ "./src/app/pages/products/products.page.scss":
/*!***************************************************!*\
  !*** ./src/app/pages/products/products.page.scss ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".border_col {\n  border: 1px solid lightgray;\n  padding: 10px;\n}\n\n.prod_img {\n  height: 200px !important;\n  width: 100%;\n}\n\n.prod_title {\n  font-weight: bold;\n  color: black;\n  font-size: 16px;\n  margin: 0px !important;\n}\n\n.prod_orgin {\n  font-size: 15px;\n  margin: 0px;\n  text-decoration: line-through;\n  color: red;\n  display: inline !important;\n}\n\n.prod_sell {\n  font-size: 15px;\n  margin: 0px;\n  color: darkolivegreen;\n  font-weight: bold;\n}\n\n.heart {\n  font-size: 20px;\n  position: absolute;\n  bottom: 0;\n  right: 0;\n}\n\n.prod_fav {\n  float: right;\n  font-size: 20px;\n}\n\n.pro_image {\n  width: 100%;\n  height: 150px;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: contain;\n}\n\nion-footer ion-col {\n  display: block;\n  text-align: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvcHJvZHVjdHMvQzpcXFVzZXJzXFxVc2VyXFxEb3dubG9hZHNcXGlvbmljNVNob3BBcHAtNXlscGRjXFxpb25pYzVTaG9wQXBwXFxBcHBfY29kZS9zcmNcXGFwcFxccGFnZXNcXHByb2R1Y3RzXFxwcm9kdWN0cy5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL3Byb2R1Y3RzL3Byb2R1Y3RzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLDJCQUFBO0VBQ0EsYUFBQTtBQ0NKOztBRENBO0VBQ0ksd0JBQUE7RUFDQSxXQUFBO0FDRUo7O0FEQUE7RUFDSSxpQkFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0Esc0JBQUE7QUNHSjs7QUREQTtFQUNJLGVBQUE7RUFDQSxXQUFBO0VBQ0EsNkJBQUE7RUFDQSxVQUFBO0VBQ0EsMEJBQUE7QUNJSjs7QURGQTtFQUNJLGVBQUE7RUFDQSxXQUFBO0VBQ0EscUJBQUE7RUFDQSxpQkFBQTtBQ0tKOztBREZBO0VBQ0ksZUFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLFFBQUE7QUNLSjs7QURIQTtFQUNJLFlBQUE7RUFDQSxlQUFBO0FDTUo7O0FESEE7RUFDSSxXQUFBO0VBQ0EsYUFBQTtFQUNBLDJCQUFBO0VBQ0EsNEJBQUE7RUFDQSx3QkFBQTtBQ01KOztBREZJO0VBQ0ksY0FBQTtFQUNBLGtCQUFBO0FDS1IiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9wcm9kdWN0cy9wcm9kdWN0cy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYm9yZGVyX2NvbHtcbiAgICBib3JkZXI6IDFweCBzb2xpZCBsaWdodGdyYXk7XG4gICAgcGFkZGluZzogMTBweDtcbn1cbi5wcm9kX2ltZ3tcbiAgICBoZWlnaHQ6IDIwMHB4ICFpbXBvcnRhbnQ7XG4gICAgd2lkdGg6IDEwMCU7XG59XG4ucHJvZF90aXRsZXtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBjb2xvcjogYmxhY2s7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICAgIG1hcmdpbjogMHB4ICFpbXBvcnRhbnQ7XG59XG4ucHJvZF9vcmdpbntcbiAgICBmb250LXNpemU6IDE1cHg7IFxuICAgIG1hcmdpbjogMHB4O1xuICAgIHRleHQtZGVjb3JhdGlvbjpsaW5lLXRocm91Z2g7XG4gICAgY29sb3I6IHJlZDtcbiAgICBkaXNwbGF5OiBpbmxpbmUgIWltcG9ydGFudDtcbn1cbi5wcm9kX3NlbGx7XG4gICAgZm9udC1zaXplOiAxNXB4OyBcbiAgICBtYXJnaW46IDBweDsgICBcbiAgICBjb2xvcjogZGFya29saXZlZ3JlZW47XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG5cbi5oZWFydHtcbiAgICBmb250LXNpemU6IDIwcHg7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJvdHRvbTogMDtcbiAgICByaWdodDogMDtcbn1cbi5wcm9kX2ZhdntcbiAgICBmbG9hdDogcmlnaHQ7XG4gICAgZm9udC1zaXplOiAyMHB4O1xufVxuXG4ucHJvX2ltYWdle1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodCA6IDE1MHB4O1xuICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcbn1cblxuaW9uLWZvb3RlcntcbiAgICBpb24tY29se1xuICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIH1cbn0iLCIuYm9yZGVyX2NvbCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkIGxpZ2h0Z3JheTtcbiAgcGFkZGluZzogMTBweDtcbn1cblxuLnByb2RfaW1nIHtcbiAgaGVpZ2h0OiAyMDBweCAhaW1wb3J0YW50O1xuICB3aWR0aDogMTAwJTtcbn1cblxuLnByb2RfdGl0bGUge1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6IGJsYWNrO1xuICBmb250LXNpemU6IDE2cHg7XG4gIG1hcmdpbjogMHB4ICFpbXBvcnRhbnQ7XG59XG5cbi5wcm9kX29yZ2luIHtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBtYXJnaW46IDBweDtcbiAgdGV4dC1kZWNvcmF0aW9uOiBsaW5lLXRocm91Z2g7XG4gIGNvbG9yOiByZWQ7XG4gIGRpc3BsYXk6IGlubGluZSAhaW1wb3J0YW50O1xufVxuXG4ucHJvZF9zZWxsIHtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBtYXJnaW46IDBweDtcbiAgY29sb3I6IGRhcmtvbGl2ZWdyZWVuO1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cblxuLmhlYXJ0IHtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJvdHRvbTogMDtcbiAgcmlnaHQ6IDA7XG59XG5cbi5wcm9kX2ZhdiB7XG4gIGZsb2F0OiByaWdodDtcbiAgZm9udC1zaXplOiAyMHB4O1xufVxuXG4ucHJvX2ltYWdlIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTUwcHg7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xufVxuXG5pb24tZm9vdGVyIGlvbi1jb2wge1xuICBkaXNwbGF5OiBibG9jaztcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/products/products.page.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/products/products.page.ts ***!
  \*************************************************/
/*! exports provided: ProductsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductsPage", function() { return ProductsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _filter_filter_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../filter/filter.page */ "./src/app/pages/filter/filter.page.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _sort_sort_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../sort/sort.page */ "./src/app/pages/sort/sort.page.ts");
/* harmony import */ var src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/dummy-data.service */ "./src/app/services/dummy-data.service.ts");

/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/






let ProductsPage = class ProductsPage {
    constructor(modalController, router, dummy) {
        this.modalController = modalController;
        this.router = router;
        this.dummy = dummy;
        this.dummyData = [];
        this.dummyData = this.dummy.products;
    }
    ngOnInit() {
    }
    onSearch() {
        console.log('search page');
    }
    onNotification() {
        console.log('notification page');
    }
    onBookmark() {
        console.log('bookmark page');
    }
    onCart() {
        console.log('cart page');
    }
    sort() {
        console.log('sort');
    }
    filter() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _filter_filter_page__WEBPACK_IMPORTED_MODULE_3__["FilterPage"],
                componentProps: { value: 123 }
            });
            yield modal.present();
        });
    }
    sorting() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _sort_sort_page__WEBPACK_IMPORTED_MODULE_5__["SortPage"],
                componentProps: { value: 123 }
            });
            yield modal.present();
        });
    }
    product() {
        this.router.navigate(['product']);
    }
};
ProductsPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_6__["DummyDataService"] }
];
ProductsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-products',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./products.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/products/products.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./products.page.scss */ "./src/app/pages/products/products.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"],
        _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
        src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_6__["DummyDataService"]])
], ProductsPage);



/***/ })

}]);
//# sourceMappingURL=pages-products-products-module-es2015.js.map