(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-add-payment-card-add-payment-card-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/add-payment-card/add-payment-card.page.html":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/add-payment-card/add-payment-card.page.html ***!
  \*********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-title>addPaymentCard</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>\n");

/***/ }),

/***/ "./src/app/pages/add-payment-card/add-payment-card.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/pages/add-payment-card/add-payment-card.module.ts ***!
  \*******************************************************************/
/*! exports provided: AddPaymentCardPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddPaymentCardPageModule", function() { return AddPaymentCardPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _add_payment_card_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./add-payment-card.page */ "./src/app/pages/add-payment-card/add-payment-card.page.ts");







const routes = [
    {
        path: '',
        component: _add_payment_card_page__WEBPACK_IMPORTED_MODULE_6__["AddPaymentCardPage"]
    }
];
let AddPaymentCardPageModule = class AddPaymentCardPageModule {
};
AddPaymentCardPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_add_payment_card_page__WEBPACK_IMPORTED_MODULE_6__["AddPaymentCardPage"]]
    })
], AddPaymentCardPageModule);



/***/ }),

/***/ "./src/app/pages/add-payment-card/add-payment-card.page.scss":
/*!*******************************************************************!*\
  !*** ./src/app/pages/add-payment-card/add-payment-card.page.scss ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2FkZC1wYXltZW50LWNhcmQvYWRkLXBheW1lbnQtY2FyZC5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/pages/add-payment-card/add-payment-card.page.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/add-payment-card/add-payment-card.page.ts ***!
  \*****************************************************************/
/*! exports provided: AddPaymentCardPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddPaymentCardPage", function() { return AddPaymentCardPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");

/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/

let AddPaymentCardPage = class AddPaymentCardPage {
    constructor() { }
    ngOnInit() {
    }
};
AddPaymentCardPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-add-payment-card',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./add-payment-card.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/add-payment-card/add-payment-card.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./add-payment-card.page.scss */ "./src/app/pages/add-payment-card/add-payment-card.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], AddPaymentCardPage);



/***/ })

}]);
//# sourceMappingURL=pages-add-payment-card-add-payment-card-module-es2015.js.map