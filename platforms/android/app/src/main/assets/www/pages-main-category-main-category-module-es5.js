function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-main-category-main-category-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/main-category/main-category.page.html":
  /*!***************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/main-category/main-category.page.html ***!
    \***************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesMainCategoryMainCategoryPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button mode=\"md\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>\n      RopApp\n    </ion-title>\n    <!-- <ion-icon slot=\"end\" (click)=\"onSearch()\" class=\"header_icons\" name=\"search\"></ion-icon> -->\n    <ion-icon slot=\"end\" (click)=\"onBookmark()\" class=\"header_icons\" name=\"bookmark\"></ion-icon>\n    <ion-icon slot=\"end\" (click)=\"onCart()\" class=\"header_icons\" name=\"basket\"></ion-icon>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"banner_slides\" [style.backgroundImage]=\"'url(assets/header/sale.jpg)'\">\n  </div>\n  <p class=\"every_text\">Prendas.</p>\n  <ion-grid fixed>\n    <ion-row>\n      <ion-col size=\"4\" *ngFor=\"let item of products\">\n        <div class=\"main_frame\" (click)=\"onCategorySelect(item)\">\n          <!-- <img [src]=\"item.img\" alt=\"\" class=\"product_img\"> -->\n          <div class=\"pro_image\" [style.backgroundImage]=\"'url(' + item.img + ')'\"></div>\n          <p class=\"header_title\">{{item.off}}</p>\n        </div>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/pages/main-category/main-category.module.ts":
  /*!*************************************************************!*\
    !*** ./src/app/pages/main-category/main-category.module.ts ***!
    \*************************************************************/

  /*! exports provided: MainCategoryPageModule */

  /***/
  function srcAppPagesMainCategoryMainCategoryModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MainCategoryPageModule", function () {
      return MainCategoryPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _main_category_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./main-category.page */
    "./src/app/pages/main-category/main-category.page.ts");

    var routes = [{
      path: '',
      component: _main_category_page__WEBPACK_IMPORTED_MODULE_6__["MainCategoryPage"]
    }];

    var MainCategoryPageModule = function MainCategoryPageModule() {
      _classCallCheck(this, MainCategoryPageModule);
    };

    MainCategoryPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_main_category_page__WEBPACK_IMPORTED_MODULE_6__["MainCategoryPage"]]
    })], MainCategoryPageModule);
    /***/
  },

  /***/
  "./src/app/pages/main-category/main-category.page.scss":
  /*!*************************************************************!*\
    !*** ./src/app/pages/main-category/main-category.page.scss ***!
    \*************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesMainCategoryMainCategoryPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".banner_slides {\n  background-position: top;\n  background-size: cover;\n  background-repeat: no-repeat;\n  height: 200px;\n  width: 100%;\n}\n\n.every_text {\n  text-align: center;\n  font-weight: bold;\n  font-size: 1rem;\n}\n\n.main_frame .product_img {\n  height: 100px;\n  width: 100%;\n  padding: 5px;\n  border: 1px solid;\n}\n\n.main_frame .pro_image {\n  width: 100%;\n  height: 100px;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: contain;\n}\n\n.main_frame .header_title {\n  font-size: 10px;\n  font-weight: bold;\n  color: black;\n  text-align: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbWFpbi1jYXRlZ29yeS9DOlxcVXNlcnNcXFVzZXJcXERvd25sb2Fkc1xcaW9uaWM1U2hvcEFwcC01eWxwZGNcXGlvbmljNVNob3BBcHBcXEFwcF9jb2RlL3NyY1xcYXBwXFxwYWdlc1xcbWFpbi1jYXRlZ29yeVxcbWFpbi1jYXRlZ29yeS5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL21haW4tY2F0ZWdvcnkvbWFpbi1jYXRlZ29yeS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSx3QkFBQTtFQUNBLHNCQUFBO0VBQ0EsNEJBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtBQ0NKOztBRENBO0VBQ0ksa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7QUNFSjs7QURDSTtFQUNJLGFBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0FDRVI7O0FEQ0k7RUFDSSxXQUFBO0VBQ0EsYUFBQTtFQUNBLDJCQUFBO0VBQ0EsNEJBQUE7RUFDQSx3QkFBQTtBQ0NSOztBREVJO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0FDQVIiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9tYWluLWNhdGVnb3J5L21haW4tY2F0ZWdvcnkucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJhbm5lcl9zbGlkZXN7XG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogdG9wO1xuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICBoZWlnaHQ6IDIwMHB4O1xuICAgIHdpZHRoOiAxMDAlO1xufVxuLmV2ZXJ5X3RleHR7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIGZvbnQtc2l6ZTogMXJlbTtcbn1cbi5tYWluX2ZyYW1le1xuICAgIC5wcm9kdWN0X2ltZ3tcbiAgICAgICAgaGVpZ2h0OiAxMDBweDtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIHBhZGRpbmc6IDVweDtcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQ7XG4gICAgfVxuXG4gICAgLnByb19pbWFnZXtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIGhlaWdodDogMTAwcHg7XG4gICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgICAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xuICAgICAgICAvLyBib3JkZXI6IDFweCBzb2xpZDtcbiAgICB9XG4gICAgLmhlYWRlcl90aXRsZXtcbiAgICAgICAgZm9udC1zaXplOiAxMHB4O1xuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgICAgY29sb3I6IGJsYWNrO1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgfVxufVxuIiwiLmJhbm5lcl9zbGlkZXMge1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiB0b3A7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGhlaWdodDogMjAwcHg7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4uZXZlcnlfdGV4dCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGZvbnQtc2l6ZTogMXJlbTtcbn1cblxuLm1haW5fZnJhbWUgLnByb2R1Y3RfaW1nIHtcbiAgaGVpZ2h0OiAxMDBweDtcbiAgd2lkdGg6IDEwMCU7XG4gIHBhZGRpbmc6IDVweDtcbiAgYm9yZGVyOiAxcHggc29saWQ7XG59XG4ubWFpbl9mcmFtZSAucHJvX2ltYWdlIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwcHg7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xufVxuLm1haW5fZnJhbWUgLmhlYWRlcl90aXRsZSB7XG4gIGZvbnQtc2l6ZTogMTBweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiBibGFjaztcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/pages/main-category/main-category.page.ts":
  /*!***********************************************************!*\
    !*** ./src/app/pages/main-category/main-category.page.ts ***!
    \***********************************************************/

  /*! exports provided: MainCategoryPage */

  /***/
  function srcAppPagesMainCategoryMainCategoryPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MainCategoryPage", function () {
      return MainCategoryPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/app/services/dummy-data.service */
    "./src/app/services/dummy-data.service.ts");
    /*
    
      Authors : initappz (Rahul Jograna)
      Website : https://initappz.com/
      Created : 17-March-2020
      This App Template Source code is licensed as per the
      terms found in the Website https://initappz.com/license
      Copyright and Good Faith Purchasers © 2020-present initappz.
    
    */


    var MainCategoryPage =
    /*#__PURE__*/
    function () {
      function MainCategoryPage(router, dummy) {
        _classCallCheck(this, MainCategoryPage);

        this.router = router;
        this.dummy = dummy;
        this.products = [];
        this.products = this.dummy.products;
      }

      _createClass(MainCategoryPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "onCategorySelect",
        value: function onCategorySelect(item) {
          console.log('item', item);
          this.router.navigate(['products']);
        }
      }, {
        key: "onSearch",
        value: function onSearch() {
          console.log('search page');
        }
      }, {
        key: "onNotification",
        value: function onNotification() {
          console.log('notification page');
        }
      }, {
        key: "onBookmark",
        value: function onBookmark() {
          console.log('bookmark page');
        }
      }, {
        key: "onCart",
        value: function onCart() {
          console.log('cart page');
        }
      }]);

      return MainCategoryPage;
    }();

    MainCategoryPage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_3__["DummyDataService"]
      }];
    };

    MainCategoryPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-main-category',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./main-category.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/main-category/main-category.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./main-category.page.scss */
      "./src/app/pages/main-category/main-category.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_3__["DummyDataService"]])], MainCategoryPage);
    /***/
  }
}]);
//# sourceMappingURL=pages-main-category-main-category-module-es5.js.map