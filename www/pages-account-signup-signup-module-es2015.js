(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-account-signup-signup-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/account/signup/signup.page.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/account/signup/signup.page.html ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header no-border>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"div_main_text\">\n    <div class=\"div_logo\">\n      <img src=\"assets/images/icon.png\" class=\"img_logo\" />\n    </div>\n    <div class=\"div_inner\">\n      <img src=\"assets/images/user_icon.png\" class=\"img_user\" />\n      <ion-input type=\"text\" Placeholder=\"Firstname\" class=\"in\"></ion-input>\n    </div>\n\n    <div class=\"div_inner\">\n      <img src=\"assets/images/user_icon.png\" class=\"img_user\" />\n      <ion-input type=\"text\" Placeholder=\"Lastname\" class=\"in\"></ion-input>\n    </div>\n\n    <div class=\"div_inner\">\n      <img src=\"assets/images/mail.png\" class=\"img_user\" />\n      <ion-input type=\"email\" Placeholder=\"Email\" class=\"in\"></ion-input>\n    </div>\n\n    <div class=\"div_inner\">\n      <img src=\"assets/images/lock.png\" class=\"img_user\" />\n      <ion-input type=\"password\" Placeholder=\"Password\" class=\"in\"></ion-input>\n    </div>\n\n    <div class=\"div_inner\">\n      <img src=\"assets/images/lock.png\" class=\"img_user\" />\n      <ion-input type=\"password\" Placeholder=\"Confirm Password\" class=\"in\"></ion-input>\n    </div>\n\n    <div class=\"div_btn\">\n      <ion-button class=\"btn_login\" expand=\"block\" mode=\"ios\" (click)=\"signup()\">Sign up</ion-button>\n    </div>\n\n    <div class=\"div_or\">\n      <ion-row>\n        <ion-col size=\"2\"></ion-col>\n        <ion-col size=\"3\">\n          <div class=\"img_line\"></div>\n        </ion-col>\n        <ion-col size=\"2\">\n          <ion-label class=\"lbl_or\">OR</ion-label>\n        </ion-col>\n        <ion-col size=\"3\">\n          <div class=\"img_line\"></div>\n        </ion-col>\n        <ion-col size=\"2\"></ion-col>\n      </ion-row>\n    </div>\n\n    <div class=\"div_social\">\n      <ion-row>\n        <ion-col size=\"6\" style=\"text-align : right;\">\n          <img src=\"assets/images/fb.png\" class=\"img_fb\" />\n        </ion-col>\n        <ion-col size=\"6\">\n          <img src=\"assets/images/gplus.png\" class=\"img_gplus\" />\n        </ion-col>\n      </ion-row>\n    </div>\n\n    <ion-label class=\"lbl_create\" (click)=\"login()\">Sign In</ion-label>\n  </div>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/account/signup/signup.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/account/signup/signup.module.ts ***!
  \*******************************************************/
/*! exports provided: SignupPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupPageModule", function() { return SignupPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _signup_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./signup.page */ "./src/app/pages/account/signup/signup.page.ts");







const routes = [
    {
        path: '',
        component: _signup_page__WEBPACK_IMPORTED_MODULE_6__["SignupPage"]
    }
];
let SignupPageModule = class SignupPageModule {
};
SignupPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_signup_page__WEBPACK_IMPORTED_MODULE_6__["SignupPage"]]
    })
], SignupPageModule);



/***/ }),

/***/ "./src/app/pages/account/signup/signup.page.scss":
/*!*******************************************************!*\
  !*** ./src/app/pages/account/signup/signup.page.scss ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".div_main_text {\n  width: 100%;\n  padding-left: 20px;\n  padding-right: 20px;\n  margin-top: 40px;\n}\n.div_main_text .div_logo {\n  width: 100%;\n  height: 15vh;\n  position: relative;\n}\n.div_main_text .div_logo .img_logo {\n  width: 105px;\n  position: relative;\n  left: 50%;\n  top: 30%;\n  -webkit-transform: translate(-50%, -50%);\n          transform: translate(-50%, -50%);\n}\n.div_main_text .div_inner {\n  margin-top: 10px;\n  height: 40px;\n  width: 100%;\n  border-radius: 5px;\n  background-color: #eeeeef;\n  position: relative;\n}\n.div_main_text .div_inner .img_user {\n  width: 15px;\n  position: absolute;\n  left: 5%;\n  top: 50%;\n  -webkit-transform: translateY(-50%);\n          transform: translateY(-50%);\n}\n.div_main_text .div_inner .in {\n  position: absolute;\n  width: 90%;\n  left: 15%;\n  top: 50%;\n  -webkit-transform: translateY(-50%);\n          transform: translateY(-50%);\n  font-size: 14px;\n}\n.div_main_text .lbl_forgot {\n  font-size: 14px;\n  display: block;\n  text-align: center;\n  margin-top: 20px;\n}\n.div_main_text .div_btn {\n  width: 100%;\n  margin-top: 20px;\n  position: relative;\n}\n.div_main_text .div_btn .btn_login {\n  color: white;\n  --background: var(--ion-color-custom);\n  --border-radius: 5px;\n}\n.div_main_text .div_or {\n  width: 100%;\n  margin-top: 20px;\n}\n.div_main_text .div_or .lbl_or {\n  position: absolute;\n  left: 50%;\n  top: 50%;\n  -webkit-transform: translate(-50%, -50%);\n          transform: translate(-50%, -50%);\n  font-size: 15px;\n}\n.div_main_text .div_or .img_line {\n  width: 100%;\n  height: 1px;\n  margin-top: 7px;\n  background-color: var(--ion-color-dark) !important;\n}\n.div_main_text .div_social {\n  margin-top: 15px;\n}\n.div_main_text .div_social .img_fb {\n  width: 50px;\n}\n.div_main_text .div_social .img_twit {\n  width: 50px;\n}\n.div_main_text .div_social .img_gplus {\n  width: 50px;\n}\n.div_main_text .lbl_create {\n  text-align: center;\n  display: block;\n  font-weight: bold;\n  color: var(--ion-color-darl);\n  font-size: 16px;\n  margin-top: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYWNjb3VudC9zaWdudXAvQzpcXFVzZXJzXFxVc2VyXFxEb3dubG9hZHNcXGlvbmljNVNob3BBcHAtNXlscGRjXFxpb25pYzVTaG9wQXBwXFxBcHBfY29kZS9zcmNcXGFwcFxccGFnZXNcXGFjY291bnRcXHNpZ251cFxcc2lnbnVwLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvYWNjb3VudC9zaWdudXAvc2lnbnVwLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQztFQWNPLFdBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7QUNaUjtBREpLO0VBQ0csV0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtBQ01SO0FESlE7RUFDSSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsUUFBQTtFQUNBLHdDQUFBO1VBQUEsZ0NBQUE7QUNNWjtBREVRO0VBQ0ksZ0JBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0EseUJBQUE7RUFDQSxrQkFBQTtBQ0FaO0FERVk7RUFDSSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxRQUFBO0VBQ0EsUUFBQTtFQUNBLG1DQUFBO1VBQUEsMkJBQUE7QUNBaEI7QURFWTtFQUNJLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFNBQUE7RUFDQSxRQUFBO0VBQ0EsbUNBQUE7VUFBQSwyQkFBQTtFQUNBLGVBQUE7QUNBaEI7QURHUTtFQUNJLGVBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBQ0RaO0FER1E7RUFDSSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtBQ0RaO0FER1k7RUFDSSxZQUFBO0VBQ0EscUNBQUE7RUFDQSxvQkFBQTtBQ0RoQjtBRElRO0VBQ0ksV0FBQTtFQUNBLGdCQUFBO0FDRlo7QURJWTtFQUNJLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLFFBQUE7RUFDQSx3Q0FBQTtVQUFBLGdDQUFBO0VBQ0EsZUFBQTtBQ0ZoQjtBRElhO0VBQ0csV0FBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0Esa0RBQUE7QUNGaEI7QURLUTtFQUNJLGdCQUFBO0FDSFo7QURJWTtFQUNJLFdBQUE7QUNGaEI7QURLWTtFQUNJLFdBQUE7QUNIaEI7QURLWTtFQUNJLFdBQUE7QUNIaEI7QURPUTtFQUNJLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsNEJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUNMWiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2FjY291bnQvc2lnbnVwL3NpZ251cC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIgLmRpdl9tYWluX3RleHR7XG4gICAgIC5kaXZfbG9nb3tcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIGhlaWdodDogMTV2aDtcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuXG4gICAgICAgIC5pbWdfbG9nb3tcbiAgICAgICAgICAgIHdpZHRoOiAxMDVweDtcbiAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgICAgICAgIGxlZnQ6IDUwJTtcbiAgICAgICAgICAgIHRvcDogMzAlO1xuICAgICAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwtNTAlKTtcbiAgICAgICAgfVxuICAgIH1cbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIHBhZGRpbmctbGVmdDogMjBweDtcbiAgICAgICAgcGFkZGluZy1yaWdodDogMjBweDtcbiAgICAgICAgbWFyZ2luLXRvcDogNDBweDtcbiBcbiAgICAgICAgLmRpdl9pbm5lcntcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XG4gICAgICAgICAgICBoZWlnaHQ6IDQwcHg7XG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNlZWVlZWY7XG4gICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG5cbiAgICAgICAgICAgIC5pbWdfdXNlcntcbiAgICAgICAgICAgICAgICB3aWR0aDogMTVweDtcbiAgICAgICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgICAgICAgICAgbGVmdDogNSU7XG4gICAgICAgICAgICAgICAgdG9wOiA1MCU7XG4gICAgICAgICAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLmlue1xuICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICAgICAgICB3aWR0aDogOTAlO1xuICAgICAgICAgICAgICAgIGxlZnQ6IDE1JTtcbiAgICAgICAgICAgICAgICB0b3A6IDUwJTtcbiAgICAgICAgICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTUwJSk7XG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIC5sYmxfZm9yZ290e1xuICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAyMHB4O1xuICAgICAgICB9XG4gICAgICAgIC5kaXZfYnRue1xuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAyMHB4O1xuICAgICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuXG4gICAgICAgICAgICAuYnRuX2xvZ2lue1xuICAgICAgICAgICAgICAgIGNvbG9yOndoaXRlO1xuICAgICAgICAgICAgICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLWN1c3RvbSk7XG4gICAgICAgICAgICAgICAgLS1ib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgLmRpdl9vcntcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAgICAgbWFyZ2luLXRvcDogMjBweDtcblxuICAgICAgICAgICAgLmxibF9vcntcbiAgICAgICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgICAgICAgICAgbGVmdDogNTAlO1xuICAgICAgICAgICAgICAgIHRvcDogNTAlO1xuICAgICAgICAgICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsLTUwJSk7XG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNXB4O1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgIC5pbWdfbGluZXtcbiAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDFweDtcbiAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiA3cHg7XG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjp2YXIoLS1pb24tY29sb3ItZGFyaykgIWltcG9ydGFudDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICAuZGl2X3NvY2lhbHtcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDE1cHg7XG4gICAgICAgICAgICAuaW1nX2Zie1xuICAgICAgICAgICAgICAgIHdpZHRoOiA1MHB4O1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLmltZ190d2l0e1xuICAgICAgICAgICAgICAgIHdpZHRoOiA1MHB4O1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLmltZ19ncGx1c3tcbiAgICAgICAgICAgICAgICB3aWR0aDogNTBweDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIC5sYmxfY3JlYXRle1xuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItZGFybCk7O1xuICAgICAgICAgICAgZm9udC1zaXplOiAxNnB4O1xuICAgICAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcbiAgICAgICAgfVxuICAgIH0iLCIuZGl2X21haW5fdGV4dCB7XG4gIHdpZHRoOiAxMDAlO1xuICBwYWRkaW5nLWxlZnQ6IDIwcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDIwcHg7XG4gIG1hcmdpbi10b3A6IDQwcHg7XG59XG4uZGl2X21haW5fdGV4dCAuZGl2X2xvZ28ge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxNXZoO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4uZGl2X21haW5fdGV4dCAuZGl2X2xvZ28gLmltZ19sb2dvIHtcbiAgd2lkdGg6IDEwNXB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGxlZnQ6IDUwJTtcbiAgdG9wOiAzMCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xufVxuLmRpdl9tYWluX3RleHQgLmRpdl9pbm5lciB7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG4gIGhlaWdodDogNDBweDtcbiAgd2lkdGg6IDEwMCU7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2VlZWVlZjtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLmRpdl9tYWluX3RleHQgLmRpdl9pbm5lciAuaW1nX3VzZXIge1xuICB3aWR0aDogMTVweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiA1JTtcbiAgdG9wOiA1MCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgtNTAlKTtcbn1cbi5kaXZfbWFpbl90ZXh0IC5kaXZfaW5uZXIgLmluIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB3aWR0aDogOTAlO1xuICBsZWZ0OiAxNSU7XG4gIHRvcDogNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTUwJSk7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cbi5kaXZfbWFpbl90ZXh0IC5sYmxfZm9yZ290IHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBkaXNwbGF5OiBibG9jaztcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tdG9wOiAyMHB4O1xufVxuLmRpdl9tYWluX3RleHQgLmRpdl9idG4ge1xuICB3aWR0aDogMTAwJTtcbiAgbWFyZ2luLXRvcDogMjBweDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLmRpdl9tYWluX3RleHQgLmRpdl9idG4gLmJ0bl9sb2dpbiB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItY3VzdG9tKTtcbiAgLS1ib3JkZXItcmFkaXVzOiA1cHg7XG59XG4uZGl2X21haW5fdGV4dCAuZGl2X29yIHtcbiAgd2lkdGg6IDEwMCU7XG4gIG1hcmdpbi10b3A6IDIwcHg7XG59XG4uZGl2X21haW5fdGV4dCAuZGl2X29yIC5sYmxfb3Ige1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDUwJTtcbiAgdG9wOiA1MCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xuICBmb250LXNpemU6IDE1cHg7XG59XG4uZGl2X21haW5fdGV4dCAuZGl2X29yIC5pbWdfbGluZSB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDFweDtcbiAgbWFyZ2luLXRvcDogN3B4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItZGFyaykgIWltcG9ydGFudDtcbn1cbi5kaXZfbWFpbl90ZXh0IC5kaXZfc29jaWFsIHtcbiAgbWFyZ2luLXRvcDogMTVweDtcbn1cbi5kaXZfbWFpbl90ZXh0IC5kaXZfc29jaWFsIC5pbWdfZmIge1xuICB3aWR0aDogNTBweDtcbn1cbi5kaXZfbWFpbl90ZXh0IC5kaXZfc29jaWFsIC5pbWdfdHdpdCB7XG4gIHdpZHRoOiA1MHB4O1xufVxuLmRpdl9tYWluX3RleHQgLmRpdl9zb2NpYWwgLmltZ19ncGx1cyB7XG4gIHdpZHRoOiA1MHB4O1xufVxuLmRpdl9tYWluX3RleHQgLmxibF9jcmVhdGUge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1kYXJsKTtcbiAgZm9udC1zaXplOiAxNnB4O1xuICBtYXJnaW4tdG9wOiAxMHB4O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/account/signup/signup.page.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/account/signup/signup.page.ts ***!
  \*****************************************************/
/*! exports provided: SignupPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupPage", function() { return SignupPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");

/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/


let SignupPage = class SignupPage {
    constructor(navCtr) {
        this.navCtr = navCtr;
    }
    ngOnInit() {
    }
    signup() {
    }
    login() {
        this.navCtr.back();
    }
};
SignupPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] }
];
SignupPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-signup',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./signup.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/account/signup/signup.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./signup.page.scss */ "./src/app/pages/account/signup/signup.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]])
], SignupPage);



/***/ })

}]);
//# sourceMappingURL=pages-account-signup-signup-module-es2015.js.map