(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home/home.page.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home/home.page.html ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>\n      RopApp\n    </ion-title>\n    <!-- <ion-icon slot=\"end\" (click)=\"onSearch()\" class=\"header_icons\" name=\"search\"></ion-icon> -->\n    <ion-icon slot=\"end\" (click)=\"onNotification()\" class=\"header_icons\" name=\"notifications-outline\"></ion-icon>\n    <ion-icon slot=\"end\" (click)=\"onBookmark()\" class=\"header_icons\" name=\"bookmark\"></ion-icon>\n    <ion-icon slot=\"end\" (click)=\"onCart()\" class=\"header_icons\" name=\"basket\"></ion-icon>\n  </ion-toolbar>\n\n</ion-header>\n\n<ion-content>\n\n  <div class=\"main_slider\">\n    <ion-slides mode=\"ios\" pager=\"ios\" class=\"banner_slider\" #mainSlider>\n      <ion-slide>\n        <div class=\"banner_slides\" [style.backgroundImage]=\"'url(assets/shop/12.jpg)'\">\n          <div class=\"overlay\">\n            <p class=\"name\">GET 50% Off</p>\n            <p class=\"explore_btn\">Explorar +</p>\n          </div>\n        </div>\n      </ion-slide>\n      <ion-slide>\n        <div class=\"banner_slides\" [style.backgroundImage]=\"'url(assets/shop/8.jpg)'\">\n          <div class=\"overlay\">\n            <p class=\"name\">80% Cashback</p>\n            <p class=\"explore_btn\">Explorar +</p>\n          </div>\n        </div>\n      </ion-slide>\n      <ion-slide>\n        <div class=\"banner_slides\" [style.backgroundImage]=\"'url(assets/shop/7.jpg)'\">\n          <div class=\"overlay\">\n            <p class=\"name\">Sale on 12:00</p>\n            <p class=\"explore_btn\">Explorar +</p>\n          </div>\n        </div>\n      </ion-slide>\n    </ion-slides>\n  </div>\n\n  <ion-slides mode=\"ios\" [options]=\"slidesOptsHeader\" margin-bottom margin-top>\n    <ion-slide *ngFor=\"let item of mainSliders;let i = index\" (click)=\"onCategorySelect(item)\">\n      <div class=\"slider_div\">\n        <div class=\"div_main\">\n          <img class=\"cat_img\" [src]=\"item.img\">\n        </div>\n        <ion-label>{{item.name}}</ion-label>\n      </div>\n    </ion-slide>\n  </ion-slides>\n\n  <!-- <ion-card id='main_img' [style.backgroundImage]=\"'url('+img+')'\">\n    <div gallery-title>\n      <p item-title text-wrap>Min. 30% Off <br>\n        ETHINIC STYLES FOR THE EXTRAODINARY</p>\n    </div>\n  </ion-card> -->\n\n  <div id='main_img' [style.backgroundImage]=\"'url('+img+')'\">\n     <div gallery-title>\n      <p item-title text-wrap>Min. 30% Off <br>\n        ETHINIC STYLES FOR THE EXTRAODINARY</p>\n    </div>\n  </div>\n\n  <!-- <ion-slides mode=\"ios\" [options]=\"slidesOptsProduct1\" margin-bottom margin-top>\n    <ion-slide *ngFor=\"let item of productSliderOne;let i = index\" (click)=\"onCategorySelect(item)\">\n      <ion-grid fixed>\n        <ion-row class=\"product_section\">\n          <ion-col size=\"6\" no-padding no-margin>\n            <div class=\"product_div\">\n              <img class=\"product_img\" [src]=\"item.img\">\n            </div>\n          </ion-col>\n          <ion-col size=\"6\">\n            <div class=\"info\">\n              <span class=\"line\"></span>\n              <p class=\"header\">{{item.header}}</p>\n              <p class=\"desc\">{{item.desc}}</p>\n              <p class=\"explore_btn\">+ EXPLORE</p>\n              <span class=\"line\"></span>\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-slide>\n  </ion-slides> -->\n\n  <ion-slides mode=\"ios\" [options]=\"slidesOptsProduct1\" margin-bottom margin-top>\n    <ion-slide *ngFor=\"let item of productSliderOne;let i = index\" (click)=\"onCategorySelect(item)\">\n      <ion-grid fixed>\n        <ion-row class=\"product_section\">\n          <ion-col size=\"6\">\n            <div class=\"product_div\" [style.backgroundImage]=\"'url( '+ item.img +' )'\"></div>\n          </ion-col>\n          <ion-col size=\"6\">\n            <div class=\"info\">\n              <span class=\"line\"></span>\n              <ion-label class=\"header\">{{item.header}}</ion-label>\n              <ion-label class=\"desc\">{{item.desc}}</ion-label>\n              <ion-label class=\"explore_btn\">+ EXPLORE</ion-label>\n              <span class=\"line\"></span>\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-slide>\n  </ion-slides>\n\n  <!-- <ion-card no-margin background-size id='main_img' [style.backgroundImage]=\"'url('+img2+')'\">\n    <div gallery-title>\n      <p item-title text-wrap>FRESH ARRIVALS <br>\n        NEW STYLES YOU'LL LOVE</p>\n    </div>\n  </ion-card> -->\n\n  <div id='main_img' [style.backgroundImage]=\"'url('+img2+')'\">\n    <div gallery-title>\n     <p item-title text-wrap>FRESH ARRIVALS <br>\n      NEW STYLES YOU'LL LOVE</p>\n   </div>\n </div>\n\n  <p class=\"every_text\">Tiendas populares.</p>\n  <ion-grid fixed>\n    <ion-row>\n      <ion-col size=\"4\" *ngFor=\"let item of forYou\">\n        <div class=\"main_frame\" (click)=\"onCategorySelect(item)\">\n          <div [style.backgroundImage]=\"'url('+item.img+')'\" class=\"product_img\"></div>\n          <p class=\"header_title\">{{item.header}}</p>\n          <p class=\"footer_title\">{{item.footer}}</p>\n        </div>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <ion-slides mode=\"ios\" [options]=\"slidesOptsProduct1\" margin-bottom margin-top>\n    <ion-slide *ngFor=\"let item of productSliderOne;let i = index\" (click)=\"onCategorySelect(item)\">\n      <ion-grid fixed>\n        <ion-row class=\"product_section\">\n          <ion-col size=\"6\">\n            <div class=\"product_div\" [style.backgroundImage]=\"'url( '+ item.img +' )'\"></div>\n          </ion-col>\n          <ion-col size=\"6\">\n            <div class=\"info\">\n              <span class=\"line\"></span>\n              <ion-label class=\"header\">{{item.header}}</ion-label>\n              <ion-label class=\"desc\">{{item.desc}}</ion-label>\n              <ion-label class=\"explore_btn\">+ EXPLORE</ion-label>\n              <span class=\"line\"></span>\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-slide>\n  </ion-slides>\n  <p class=\"every_text\">Tiendas deportivas.</p>\n  <ion-grid fixed>\n    <ion-row>\n      <ion-col size=\"4\" *ngFor=\"let item of forYou\">\n        <div class=\"main_frame\" (click)=\"onCategorySelect(item)\">\n          <div [style.backgroundImage]=\"'url('+item.img+')'\" class=\"product_img\"></div>\n          <p class=\"header_title\">{{item.header}}</p>\n          <p class=\"footer_title\">{{item.footer}}</p>\n        </div>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  <p class=\"every_text\">Tiendas Playeras.</p>\n  <ion-grid fixed>\n    <ion-row>\n      <ion-col size=\"4\" *ngFor=\"let item of player\">\n        <div class=\"main_frame\" (click)=\"onCategorySelect(item)\">\n          <div [style.backgroundImage]=\"'url('+item.img+')'\" class=\"product_img\"></div>\n          <p class=\"header_title\">{{item.header}}</p>\n          <p class=\"footer_title\">{{item.footer}}</p>\n        </div>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  <p class=\"every_text\">Otros.</p>\n  <ion-grid fixed>\n    <ion-row>\n      <ion-col size=\"4\" *ngFor=\"let item of otro\">\n        <div class=\"main_frame\" (click)=\"onCategorySelect(item)\">\n          <div [style.backgroundImage]=\"'url('+item.img+')'\" class=\"product_img\"></div>\n          <p class=\"header_title\">{{item.header}}</p>\n          <p class=\"footer_title\">{{item.footer}}</p>\n        </div>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  \n  <!-- <ion-card no-margin background-size id='main_img' [style.backgroundImage]=\"'url('+img+')'\">\n    <div gallery-title>\n      <p item-title text-wrap>Min. 30% Off <br>\n        ETHINIC STYLES FOR THE EXTRAODINARY</p>\n    </div>\n  </ion-card> -->\n\n  <div id='main_img' [style.backgroundImage]=\"'url('+img+')'\">\n    <div gallery-title>\n     <p item-title text-wrap>Min. 30% Off <br>\n      ETHINIC STYLES FOR THE EXTRAODINARY</p>\n   </div>\n </div>\n\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/home/home.module.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/home/home.module.ts ***!
  \*******************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home.page */ "./src/app/pages/home/home.page.ts");







// import { DirectivesModule } from 'src/app/directives/directives.module';
// import { SharedModule } from 'src/app/shared.module';
const routes = [
    {
        path: '',
        component: _home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]
    }
];
let HomePageModule = class HomePageModule {
};
HomePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
        ],
        declarations: [_home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]]
    })
], HomePageModule);



/***/ }),

/***/ "./src/app/pages/home/home.page.scss":
/*!*******************************************!*\
  !*** ./src/app/pages/home/home.page.scss ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".header_icons {\n  font-size: 20px;\n  padding: 10px;\n}\n\n.main_slider {\n  width: 100%;\n  height: 200px;\n  position: relative;\n}\n\n.main_slider .banner_slides {\n  background-position: top;\n  background-size: cover;\n  background-repeat: no-repeat;\n  height: 200px;\n  width: 100%;\n}\n\n.main_slider .overlay {\n  position: absolute;\n  left: 0;\n  right: 0;\n  padding: 2px;\n  bottom: 0;\n  min-height: 45px;\n  border-bottom: 1px solid transparent !important;\n  overflow: hidden;\n  background-image: linear-gradient(-230deg, rgba(76, 30, 30, 0.4), #4c2828);\n}\n\n.main_slider .overlay .name {\n  font-size: 10px;\n  text-align: right;\n  color: white;\n  margin: 2px;\n}\n\n.main_slider .overlay .explore_btn {\n  margin: 2px;\n  font-size: 10px;\n  text-align: right;\n  color: white;\n}\n\n.main_slider .leftArrow {\n  width: 20px;\n  position: absolute;\n  top: 50%;\n  z-index: 999;\n  left: 10px;\n}\n\n.main_slider .rightArrow {\n  width: 20px;\n  position: absolute;\n  top: 50%;\n  z-index: 999;\n  right: 10px !important;\n}\n\n.slider_div {\n  border: 1px solid lightgray;\n  border-radius: 5px;\n}\n\n.slider_div ion-label {\n  margin-top: 5px;\n  font-size: 11px;\n  font-weight: bolder;\n}\n\n.slider_div .div_main {\n  height: 75px !important;\n  width: 95px !important;\n  position: relative;\n  border-bottom: 1px solid lightgray;\n}\n\n.slider_div .div_main .cat_img {\n  height: 30px;\n  width: 30px;\n  position: absolute;\n  left: 50%;\n  top: 50%;\n  -webkit-transform: translate(-50%, -50%);\n          transform: translate(-50%, -50%);\n}\n\n#main_img {\n  width: 96%;\n  display: block;\n  margin: auto;\n  height: 300px;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n  margin-top: 15px;\n  margin-bottom: 15px;\n  position: relative;\n  box-shadow: 0 0 18px rgba(0, 0, 0, 0.3);\n}\n\n[gallery-title] {\n  position: absolute;\n  left: 0;\n  right: 0;\n  padding: 2px;\n  bottom: 0;\n  min-height: 45px;\n  border-bottom: 1px solid transparent !important;\n  overflow: hidden;\n  background-image: linear-gradient(-230deg, rgba(0, 0, 0, 0.4), #040404);\n}\n\n[gallery-title] [item-title], [gallery-title] [item-subtitle] {\n  color: white !important;\n  font-weight: bold !important;\n  text-align: center;\n}\n\n.product_section {\n  box-shadow: 0 0 7px rgba(0, 0, 0, 0.3);\n  border: 1px solid lightgray;\n}\n\n.product_section ion-label {\n  display: block;\n  margin-bottom: 10px;\n}\n\n.product_section .product_div {\n  width: 100%;\n  height: 100%;\n  background-position: center;\n  background-size: cover;\n  background-repeat: no-repeat;\n}\n\n.product_section .info {\n  margin-left: 10px;\n}\n\n.product_section .info .line {\n  width: 20%;\n  margin-top: 10px;\n  margin-bottom: 10px;\n  height: 2px;\n  background-color: gray;\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: left;\n          justify-content: left;\n}\n\n.product_section .info .header {\n  font-size: 10px;\n  text-align: left;\n  color: gray;\n}\n\n.product_section .info .desc {\n  font-size: 12px;\n  text-align: left;\n  font-weight: bold;\n}\n\n.product_section .info .explore_btn {\n  font-size: 10px;\n  text-align: left;\n  color: gray;\n}\n\n.every_text {\n  text-align: center;\n  font-weight: bold;\n  font-size: 1rem;\n}\n\n.main_frame .product_img {\n  height: 100px;\n  width: 100%;\n  padding: 5px;\n  border: 1px solid lightgray;\n  background-repeat: no-repeat;\n  background-position: center;\n  background-size: cover;\n}\n\n.main_frame .header_title {\n  font-size: 10px;\n  font-weight: bold;\n  color: black;\n}\n\n.main_frame .footer_title {\n  font-size: 8px;\n  color: gray;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvaG9tZS9DOlxcVXNlcnNcXFVzZXJcXERvd25sb2Fkc1xcaW9uaWM1U2hvcEFwcC01eWxwZGNcXGlvbmljNVNob3BBcHBcXEFwcF9jb2RlL3NyY1xcYXBwXFxwYWdlc1xcaG9tZVxcaG9tZS5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2hvbWUvaG9tZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxlQUFBO0VBQ0EsYUFBQTtBQ0NKOztBREVBO0VBQ0ksV0FBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtBQ0NKOztBRENJO0VBQ0ksd0JBQUE7RUFDQSxzQkFBQTtFQUNBLDRCQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7QUNDUjs7QURDSTtFQUNJLGtCQUFBO0VBQ0EsT0FBQTtFQUNBLFFBQUE7RUFDQSxZQUFBO0VBQ0EsU0FBQTtFQUNBLGdCQUFBO0VBQ0EsK0NBQUE7RUFDQSxnQkFBQTtFQUNBLDBFQUFBO0FDQ1I7O0FEQVE7RUFDSSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtBQ0VaOztBREFTO0VBQ0csV0FBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLFlBQUE7QUNFWjs7QURDSTtFQUNJLFdBQUE7RUFDQSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtBQ0NSOztBRENJO0VBQ0ksV0FBQTtFQUNBLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFlBQUE7RUFDQSxzQkFBQTtBQ0NSOztBREdBO0VBQ0ksMkJBQUE7RUFDQSxrQkFBQTtBQ0FKOztBRENJO0VBQ0ksZUFBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtBQ0NSOztBRENJO0VBQ0ksdUJBQUE7RUFDQSxzQkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0NBQUE7QUNDUjs7QURBUTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsUUFBQTtFQUNBLHdDQUFBO1VBQUEsZ0NBQUE7QUNFWjs7QURFQztFQUNHLFVBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSwyQkFBQTtFQUNBLDRCQUFBO0VBQ0Esc0JBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSx1Q0FBQTtBQ0NKOztBREVFO0VBQ0Usa0JBQUE7RUFDQSxPQUFBO0VBQ0EsUUFBQTtFQUNBLFlBQUE7RUFDQSxTQUFBO0VBQ0EsZ0JBQUE7RUFDQSwrQ0FBQTtFQUNBLGdCQUFBO0VBQ0EsdUVBQUE7QUNDSjs7QURBSTtFQUNJLHVCQUFBO0VBQ0EsNEJBQUE7RUFDQSxrQkFBQTtBQ0VSOztBREVBO0VBQ0ksc0NBQUE7RUFDQSwyQkFBQTtBQ0NKOztBRENJO0VBQ0ksY0FBQTtFQUNBLG1CQUFBO0FDQ1I7O0FERUk7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLDJCQUFBO0VBQ0Esc0JBQUE7RUFDQSw0QkFBQTtBQ0FSOztBREVJO0VBQ0ksaUJBQUE7QUNBUjs7QURDUTtFQUNRLFVBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLHNCQUFBO0VBQ0Esb0JBQUE7RUFBQSxhQUFBO0VBQ0Esc0JBQUE7VUFBQSxxQkFBQTtBQ0NoQjs7QURDWTtFQUNJLGVBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7QUNDaEI7O0FEQ1k7RUFDSSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtBQ0NoQjs7QURDWTtFQUNJLGVBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7QUNDaEI7O0FESUE7RUFDSSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtBQ0RKOztBRElJO0VBQ0ksYUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsMkJBQUE7RUFFQSw0QkFBQTtFQUNBLDJCQUFBO0VBQ0Esc0JBQUE7QUNGUjs7QURJSTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtFQUNBLFlBQUE7QUNGUjs7QURJSTtFQUNJLGNBQUE7RUFDQSxXQUFBO0FDRlIiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9ob21lL2hvbWUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmhlYWRlcl9pY29uc3tcbiAgICBmb250LXNpemU6IDIwcHg7XG4gICAgcGFkZGluZzogMTBweDtcbn1cblxuLm1haW5fc2xpZGVye1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMjAwcHg7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuXG4gICAgLmJhbm5lcl9zbGlkZXN7XG4gICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IHRvcDtcbiAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICAgICAgaGVpZ2h0OiAyMDBweDtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgfVxuICAgIC5vdmVybGF5e1xuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgIGxlZnQ6IDA7XG4gICAgICAgIHJpZ2h0OiAwO1xuICAgICAgICBwYWRkaW5nOiAycHg7XG4gICAgICAgIGJvdHRvbTogMDtcbiAgICAgICAgbWluLWhlaWdodDogNDVweDtcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHRyYW5zcGFyZW50ICFpbXBvcnRhbnQ7XG4gICAgICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgICAgIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCgtMjMwZGVnLCByZ2JhKDc2LCAzMCwgMzAsIDAuNCksICM0YzI4MjgpO1xuICAgICAgICAubmFtZXtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTBweDtcbiAgICAgICAgICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xuICAgICAgICAgICAgbWFyZ2luOiAycHg7XG4gICAgICAgICB9XG4gICAgICAgICAuZXhwbG9yZV9idG57XG4gICAgICAgICAgICBtYXJnaW46IDJweDtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTBweDtcbiAgICAgICAgICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xuICAgICAgICB9XG4gICAgfVxuICAgIC5sZWZ0QXJyb3d7XG4gICAgICAgIHdpZHRoOiAyMHB4O1xuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgIHRvcDogNTAlO1xuICAgICAgICB6LWluZGV4OiA5OTk7XG4gICAgICAgIGxlZnQ6IDEwcHg7XG4gICAgfVxuICAgIC5yaWdodEFycm93e1xuICAgICAgICB3aWR0aDogMjBweDtcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICB0b3A6IDUwJTtcbiAgICAgICAgei1pbmRleDogOTk5O1xuICAgICAgICByaWdodDogMTBweCAhaW1wb3J0YW50O1xuICAgIH1cbn1cblxuLnNsaWRlcl9kaXZ7XG4gICAgYm9yZGVyOiAxcHggc29saWQgbGlnaHRncmF5O1xuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICBpb24tbGFiZWx7XG4gICAgICAgIG1hcmdpbi10b3A6IDVweDtcbiAgICAgICAgZm9udC1zaXplOiAxMXB4O1xuICAgICAgICBmb250LXdlaWdodDogYm9sZGVyO1xuICAgIH1cbiAgICAuZGl2X21haW57XG4gICAgICAgIGhlaWdodDogNzVweCAhaW1wb3J0YW50O1xuICAgICAgICB3aWR0aDogOTVweCAhaW1wb3J0YW50O1xuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBsaWdodGdyYXk7XG4gICAgICAgIC5jYXRfaW1ne1xuICAgICAgICAgICAgaGVpZ2h0OiAzMHB4O1xuICAgICAgICAgICAgd2lkdGg6IDMwcHg7XG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgICAgICBsZWZ0OiA1MCU7XG4gICAgICAgICAgICB0b3A6IDUwJTtcbiAgICAgICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsLTUwJSlcbiAgICAgICAgfVxuICAgIH1cbn0gXG4gI21haW5faW1nIHtcbiAgICB3aWR0aDogOTYlO1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIG1hcmdpbjogYXV0bzsgICBcbiAgICBoZWlnaHQ6IDMwMHB4O1xuICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgbWFyZ2luLXRvcDogMTVweDtcbiAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBib3gtc2hhZG93OiAwIDAgMThweCByZ2JhKDAsIDAsIDAsIDAuMyk7XG59XG5cbiAgW2dhbGxlcnktdGl0bGVdIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgbGVmdDogMDtcbiAgICByaWdodDogMDtcbiAgICBwYWRkaW5nOiAycHg7XG4gICAgYm90dG9tOiAwO1xuICAgIG1pbi1oZWlnaHQ6IDQ1cHg7XG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHRyYW5zcGFyZW50ICFpbXBvcnRhbnQ7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoLTIzMGRlZywgcmdiYSgwLCAwLCAwLCAwLjQpLCAjMDQwNDA0KTtcbiAgICBbaXRlbS10aXRsZV0sW2l0ZW0tc3VidGl0bGVdIHtcbiAgICAgICAgY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkICFpbXBvcnRhbnQ7XG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB9XG4gIH1cblxuLnByb2R1Y3Rfc2VjdGlvbntcbiAgICBib3gtc2hhZG93OiAwIDAgN3B4IHJnYmEoMCwgMCwgMCwgMC4zKTtcbiAgICBib3JkZXI6IDFweCBzb2xpZCBsaWdodGdyYXk7XG5cbiAgICBpb24tbGFiZWx7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICAgIH1cblxuICAgIC5wcm9kdWN0X2RpdntcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIGhlaWdodDogMTAwJTtcbiAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgIH1cbiAgICAuaW5mb3tcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gICAgICAgIC5saW5le1xuICAgICAgICAgICAgICAgIHdpZHRoOiAyMCU7XG4gICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcbiAgICAgICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICAgICAgICAgICAgICAgIGhlaWdodDoycHg7XG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogZ3JheTtcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogbGVmdDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC5oZWFkZXJ7XG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxMHB4O1xuICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgICAgICAgICAgICAgY29sb3I6IGdyYXk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAuZGVzY3tcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDEycHg7XG4gICAgICAgICAgICAgICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC5leHBsb3JlX2J0bntcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDEwcHg7XG4gICAgICAgICAgICAgICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICAgICAgICAgICAgICBjb2xvcjogZ3JheTtcbiAgICAgICAgICAgIH1cbiAgICB9XG59XG5cbi5ldmVyeV90ZXh0e1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBmb250LXNpemU6IDFyZW07XG59XG4ubWFpbl9mcmFtZXtcbiAgICAucHJvZHVjdF9pbWd7XG4gICAgICAgIGhlaWdodDogMTAwcHg7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBwYWRkaW5nOiA1cHg7XG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkIGxpZ2h0Z3JheTtcblxuICAgICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gICAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgfVxuICAgIC5oZWFkZXJfdGl0bGV7XG4gICAgICAgIGZvbnQtc2l6ZTogMTBweDtcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgICAgIGNvbG9yOiBibGFjaztcbiAgICB9XG4gICAgLmZvb3Rlcl90aXRsZXtcbiAgICAgICAgZm9udC1zaXplOiA4cHg7XG4gICAgICAgIGNvbG9yOiBncmF5O1xuICAgIH1cbn0iLCIuaGVhZGVyX2ljb25zIHtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBwYWRkaW5nOiAxMHB4O1xufVxuXG4ubWFpbl9zbGlkZXIge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAyMDBweDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLm1haW5fc2xpZGVyIC5iYW5uZXJfc2xpZGVzIHtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogdG9wO1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBoZWlnaHQ6IDIwMHB4O1xuICB3aWR0aDogMTAwJTtcbn1cbi5tYWluX3NsaWRlciAub3ZlcmxheSB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogMDtcbiAgcmlnaHQ6IDA7XG4gIHBhZGRpbmc6IDJweDtcbiAgYm90dG9tOiAwO1xuICBtaW4taGVpZ2h0OiA0NXB4O1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgdHJhbnNwYXJlbnQgIWltcG9ydGFudDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KC0yMzBkZWcsIHJnYmEoNzYsIDMwLCAzMCwgMC40KSwgIzRjMjgyOCk7XG59XG4ubWFpbl9zbGlkZXIgLm92ZXJsYXkgLm5hbWUge1xuICBmb250LXNpemU6IDEwcHg7XG4gIHRleHQtYWxpZ246IHJpZ2h0O1xuICBjb2xvcjogd2hpdGU7XG4gIG1hcmdpbjogMnB4O1xufVxuLm1haW5fc2xpZGVyIC5vdmVybGF5IC5leHBsb3JlX2J0biB7XG4gIG1hcmdpbjogMnB4O1xuICBmb250LXNpemU6IDEwcHg7XG4gIHRleHQtYWxpZ246IHJpZ2h0O1xuICBjb2xvcjogd2hpdGU7XG59XG4ubWFpbl9zbGlkZXIgLmxlZnRBcnJvdyB7XG4gIHdpZHRoOiAyMHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogNTAlO1xuICB6LWluZGV4OiA5OTk7XG4gIGxlZnQ6IDEwcHg7XG59XG4ubWFpbl9zbGlkZXIgLnJpZ2h0QXJyb3cge1xuICB3aWR0aDogMjBweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDUwJTtcbiAgei1pbmRleDogOTk5O1xuICByaWdodDogMTBweCAhaW1wb3J0YW50O1xufVxuXG4uc2xpZGVyX2RpdiB7XG4gIGJvcmRlcjogMXB4IHNvbGlkIGxpZ2h0Z3JheTtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xufVxuLnNsaWRlcl9kaXYgaW9uLWxhYmVsIHtcbiAgbWFyZ2luLXRvcDogNXB4O1xuICBmb250LXNpemU6IDExcHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG59XG4uc2xpZGVyX2RpdiAuZGl2X21haW4ge1xuICBoZWlnaHQ6IDc1cHggIWltcG9ydGFudDtcbiAgd2lkdGg6IDk1cHggIWltcG9ydGFudDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgbGlnaHRncmF5O1xufVxuLnNsaWRlcl9kaXYgLmRpdl9tYWluIC5jYXRfaW1nIHtcbiAgaGVpZ2h0OiAzMHB4O1xuICB3aWR0aDogMzBweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiA1MCU7XG4gIHRvcDogNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcbn1cblxuI21haW5faW1nIHtcbiAgd2lkdGg6IDk2JTtcbiAgZGlzcGxheTogYmxvY2s7XG4gIG1hcmdpbjogYXV0bztcbiAgaGVpZ2h0OiAzMDBweDtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICBtYXJnaW4tdG9wOiAxNXB4O1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGJveC1zaGFkb3c6IDAgMCAxOHB4IHJnYmEoMCwgMCwgMCwgMC4zKTtcbn1cblxuW2dhbGxlcnktdGl0bGVdIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiAwO1xuICByaWdodDogMDtcbiAgcGFkZGluZzogMnB4O1xuICBib3R0b206IDA7XG4gIG1pbi1oZWlnaHQ6IDQ1cHg7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCB0cmFuc3BhcmVudCAhaW1wb3J0YW50O1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoLTIzMGRlZywgcmdiYSgwLCAwLCAwLCAwLjQpLCAjMDQwNDA0KTtcbn1cbltnYWxsZXJ5LXRpdGxlXSBbaXRlbS10aXRsZV0sIFtnYWxsZXJ5LXRpdGxlXSBbaXRlbS1zdWJ0aXRsZV0ge1xuICBjb2xvcjogd2hpdGUgIWltcG9ydGFudDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQgIWltcG9ydGFudDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4ucHJvZHVjdF9zZWN0aW9uIHtcbiAgYm94LXNoYWRvdzogMCAwIDdweCByZ2JhKDAsIDAsIDAsIDAuMyk7XG4gIGJvcmRlcjogMXB4IHNvbGlkIGxpZ2h0Z3JheTtcbn1cbi5wcm9kdWN0X3NlY3Rpb24gaW9uLWxhYmVsIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG59XG4ucHJvZHVjdF9zZWN0aW9uIC5wcm9kdWN0X2RpdiB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbn1cbi5wcm9kdWN0X3NlY3Rpb24gLmluZm8ge1xuICBtYXJnaW4tbGVmdDogMTBweDtcbn1cbi5wcm9kdWN0X3NlY3Rpb24gLmluZm8gLmxpbmUge1xuICB3aWR0aDogMjAlO1xuICBtYXJnaW4tdG9wOiAxMHB4O1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICBoZWlnaHQ6IDJweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogZ3JheTtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBsZWZ0O1xufVxuLnByb2R1Y3Rfc2VjdGlvbiAuaW5mbyAuaGVhZGVyIHtcbiAgZm9udC1zaXplOiAxMHB4O1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBjb2xvcjogZ3JheTtcbn1cbi5wcm9kdWN0X3NlY3Rpb24gLmluZm8gLmRlc2Mge1xuICBmb250LXNpemU6IDEycHg7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLnByb2R1Y3Rfc2VjdGlvbiAuaW5mbyAuZXhwbG9yZV9idG4ge1xuICBmb250LXNpemU6IDEwcHg7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIGNvbG9yOiBncmF5O1xufVxuXG4uZXZlcnlfdGV4dCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGZvbnQtc2l6ZTogMXJlbTtcbn1cblxuLm1haW5fZnJhbWUgLnByb2R1Y3RfaW1nIHtcbiAgaGVpZ2h0OiAxMDBweDtcbiAgd2lkdGg6IDEwMCU7XG4gIHBhZGRpbmc6IDVweDtcbiAgYm9yZGVyOiAxcHggc29saWQgbGlnaHRncmF5O1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG59XG4ubWFpbl9mcmFtZSAuaGVhZGVyX3RpdGxlIHtcbiAgZm9udC1zaXplOiAxMHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6IGJsYWNrO1xufVxuLm1haW5fZnJhbWUgLmZvb3Rlcl90aXRsZSB7XG4gIGZvbnQtc2l6ZTogOHB4O1xuICBjb2xvcjogZ3JheTtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/pages/home/home.page.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/home/home.page.ts ***!
  \*****************************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/dummy-data.service */ "./src/app/services/dummy-data.service.ts");

/*

  Authors : (Xavier Siavichay & Josue Vargas)
  Website : https://initappz.com/
  Created : 17-March-2020
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/




let HomePage = class HomePage {
    constructor(router, dummy) {
        this.router = router;
        this.dummy = dummy;
        this.slidesOptsHeader = {
            slidesPerView: 4,
        };
        this.slidesOptsProduct1 = {
            slidesPerView: 1.3,
        };
        this.img = 'https://m.zeleb.es/sites/default/files/esta_noche_te_iras_de_fiesta_con_ropa_de_bershka_y_hm_es_un_nuevo_mandamiento.jpg';
        this.img2 = 'https://estaticos.efe.com/efecom/recursos2/imagen.aspx?lVW2oAh2vjO1-P-2fHQqc30OJTzIirbD2HatQ4TncnkXVSTX-P-2bAoG0sxzXPZPAk5l-P-2fU5UzjGylKWxGo9qRqiBrS1vRA-P-3d-P-3d';
        this.mainSliders = [];
        this.productSliderOne = [];
        this.forYou = [];
        this.player = [];
        this.otro = [];
        this.mainSliders = this.dummy.catSlider;
        this.productSliderOne = this.dummy.productSlider;
        this.forYou = this.dummy.specialForYou;
        this.player = this.dummy.playera;
        this.otro = this.dummy.otro;
    }
    ngOnInit() {
    }
    onSearch() {
        console.log('search page');
    }
    onNotification() {
        console.log('notification page');
    }
    onBookmark() {
        console.log('bookmark page');
    }
    onCart() {
        console.log('cart page');
    }
    onCategorySelect(item) {
        console.log('item', item);
        this.router.navigate(['main-category']);
    }
};
HomePage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_4__["DummyDataService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('mainSlider', { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonSlides"])
], HomePage.prototype, "mainSlider", void 0);
HomePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./home.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home/home.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./home.page.scss */ "./src/app/pages/home/home.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
        src_app_services_dummy_data_service__WEBPACK_IMPORTED_MODULE_4__["DummyDataService"]])
], HomePage);



/***/ })

}]);
//# sourceMappingURL=home-home-module-es2015.js.map