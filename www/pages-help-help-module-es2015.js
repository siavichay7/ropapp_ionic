(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-help-help-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/help/help.page.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/help/help.page.html ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>Help</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"bg-light\">\n\n  <div class=\"prod_main\">\n    <ion-label class=\"title\">FAQ</ion-label>\n  </div>\n  \n  <ion-card>\n    <ion-card-header>\n      Order tracking and Delivery\n      <ion-icon name=\"arrow-forward\" float-right></ion-icon>\n    </ion-card-header>\n    <ion-card-content class=\"text-light\">\n      Lorem Ipsum is simply dummy text of the printing and typesetting industry.\n    </ion-card-content>\n  </ion-card>\n\n  <ion-card>\n    <ion-card-header>\n      Refund\n      <ion-icon name=\"arrow-forward\" float-right></ion-icon>\n    </ion-card-header>\n    <ion-card-content class=\"text-light\">\n      Lorem Ipsum is simply dummy text of the printing and typesetting industry.\n    </ion-card-content>\n  </ion-card>\n\n  <ion-card>\n    <ion-card-header>\n      Cancellation of orders\n      <ion-icon name=\"arrow-forward\" float-right></ion-icon>\n    </ion-card-header>\n    <ion-card-content class=\"text-light\">\n      Lorem Ipsum is simply dummy text of the printing and typesetting industry.\n    </ion-card-content>\n  </ion-card>\n\n  <ion-card>\n    <ion-card-header>\n      Seller Support\n      <ion-icon name=\"arrow-forward\" float-right></ion-icon>\n    </ion-card-header>\n    <ion-card-content class=\"text-light\">\n      Lorem Ipsum is simply dummy text of the printing and typesetting industry.\n    </ion-card-content>\n  </ion-card>\n\n  <ion-card>\n    <ion-card-header>\n      Payments\n      <ion-icon name=\"arrow-forward\" float-right></ion-icon>\n    </ion-card-header>\n    <ion-card-content class=\"text-light\">\n      Lorem Ipsum is simply dummy text of the printing and typesetting industry.\n    </ion-card-content>\n  </ion-card>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/help/help.module.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/help/help.module.ts ***!
  \*******************************************/
/*! exports provided: HelpPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HelpPageModule", function() { return HelpPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _help_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./help.page */ "./src/app/pages/help/help.page.ts");







const routes = [
    {
        path: '',
        component: _help_page__WEBPACK_IMPORTED_MODULE_6__["HelpPage"]
    }
];
let HelpPageModule = class HelpPageModule {
};
HelpPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_help_page__WEBPACK_IMPORTED_MODULE_6__["HelpPage"]]
    })
], HelpPageModule);



/***/ }),

/***/ "./src/app/pages/help/help.page.scss":
/*!*******************************************!*\
  !*** ./src/app/pages/help/help.page.scss ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".card {\n  border-radius: 5px !important;\n  box-shadow: 0px 1px 3px rgba(10, 10, 10, 0.2);\n}\n.card .card-header {\n  font-size: 1.3rem;\n  color: #000;\n  padding: 16px;\n  font-weight: 500;\n  padding-bottom: 8px;\n}\n.card .card-header ion-icon {\n  font-size: 1.1em;\n  color: #4c4c4c;\n}\n.card .card-content {\n  font-size: 1.1rem;\n  font-weight: 300;\n}\n.card .card-content.text-light {\n  color: #858585 !important;\n}\n.prod_main {\n  box-shadow: 9px 3px 16px 9px rgba(126, 126, 171, 0.2);\n  padding: 15px;\n  margin-bottom: 20px;\n}\n.prod_main .title {\n  font-weight: bolder;\n  font-size: 20px;\n  padding: 0px;\n  color: black;\n  margin: 0px !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvaGVscC9DOlxcVXNlcnNcXFVzZXJcXERvd25sb2Fkc1xcaW9uaWM1U2hvcEFwcC01eWxwZGNcXGlvbmljNVNob3BBcHBcXEFwcF9jb2RlL3NyY1xcYXBwXFxwYWdlc1xcaGVscFxcaGVscC5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2hlbHAvaGVscC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0E7RUFDSSw2QkFBQTtFQUNBLDZDQUFBO0FDQUo7QURDSTtFQUNJLGlCQUFBO0VBQ0EsV0FBQTtFQUNBLGFBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0FDQ1I7QURBUTtFQUNJLGdCQUFBO0VBQ0EsY0FBQTtBQ0VaO0FEQ0k7RUFDSSxpQkFBQTtFQUNBLGdCQUFBO0FDQ1I7QURBUTtFQUNJLHlCQUFBO0FDRVo7QURHQTtFQUNJLHFEQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0FDQUo7QURFSTtFQUNJLG1CQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0Esc0JBQUE7QUNBUiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2hlbHAvaGVscC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcbi5jYXJkIHtcbiAgICBib3JkZXItcmFkaXVzOiA1cHggIWltcG9ydGFudDtcbiAgICBib3gtc2hhZG93OiAwcHggMXB4IDNweCByZ2JhKDEwLCAxMCwgMTAsIDAuMik7XG4gICAgLmNhcmQtaGVhZGVyIHtcbiAgICAgICAgZm9udC1zaXplOiAxLjNyZW07XG4gICAgICAgIGNvbG9yOiAjMDAwO1xuICAgICAgICBwYWRkaW5nOiAxNnB4O1xuICAgICAgICBmb250LXdlaWdodDogNTAwO1xuICAgICAgICBwYWRkaW5nLWJvdHRvbTogOHB4O1xuICAgICAgICBpb24taWNvbiB7XG4gICAgICAgICAgICBmb250LXNpemU6IDEuMWVtO1xuICAgICAgICAgICAgY29sb3I6ICM0YzRjNGM7XG4gICAgICAgIH1cbiAgICB9XG4gICAgLmNhcmQtY29udGVudCB7XG4gICAgICAgIGZvbnQtc2l6ZTogMS4xcmVtO1xuICAgICAgICBmb250LXdlaWdodDogMzAwO1xuICAgICAgICAmLnRleHQtbGlnaHQge1xuICAgICAgICAgICAgY29sb3I6ICM4NTg1ODUgIWltcG9ydGFudDtcbiAgICAgICAgfVxuICAgIH1cbn1cblxuLnByb2RfbWFpbntcbiAgICBib3gtc2hhZG93OiA5cHggM3B4IDE2cHggOXB4IHJnYmEoMTI2LCAxMjYsIDE3MSwgMC4yKTtcbiAgICBwYWRkaW5nOiAxNXB4O1xuICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XG5cbiAgICAudGl0bGV7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG4gICAgICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICAgICAgcGFkZGluZzogMHB4O1xuICAgICAgICBjb2xvcjogYmxhY2s7XG4gICAgICAgIG1hcmdpbjogMHB4ICFpbXBvcnRhbnQ7XG4gICAgfVxufSIsIi5jYXJkIHtcbiAgYm9yZGVyLXJhZGl1czogNXB4ICFpbXBvcnRhbnQ7XG4gIGJveC1zaGFkb3c6IDBweCAxcHggM3B4IHJnYmEoMTAsIDEwLCAxMCwgMC4yKTtcbn1cbi5jYXJkIC5jYXJkLWhlYWRlciB7XG4gIGZvbnQtc2l6ZTogMS4zcmVtO1xuICBjb2xvcjogIzAwMDtcbiAgcGFkZGluZzogMTZweDtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgcGFkZGluZy1ib3R0b206IDhweDtcbn1cbi5jYXJkIC5jYXJkLWhlYWRlciBpb24taWNvbiB7XG4gIGZvbnQtc2l6ZTogMS4xZW07XG4gIGNvbG9yOiAjNGM0YzRjO1xufVxuLmNhcmQgLmNhcmQtY29udGVudCB7XG4gIGZvbnQtc2l6ZTogMS4xcmVtO1xuICBmb250LXdlaWdodDogMzAwO1xufVxuLmNhcmQgLmNhcmQtY29udGVudC50ZXh0LWxpZ2h0IHtcbiAgY29sb3I6ICM4NTg1ODUgIWltcG9ydGFudDtcbn1cblxuLnByb2RfbWFpbiB7XG4gIGJveC1zaGFkb3c6IDlweCAzcHggMTZweCA5cHggcmdiYSgxMjYsIDEyNiwgMTcxLCAwLjIpO1xuICBwYWRkaW5nOiAxNXB4O1xuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xufVxuLnByb2RfbWFpbiAudGl0bGUge1xuICBmb250LXdlaWdodDogYm9sZGVyO1xuICBmb250LXNpemU6IDIwcHg7XG4gIHBhZGRpbmc6IDBweDtcbiAgY29sb3I6IGJsYWNrO1xuICBtYXJnaW46IDBweCAhaW1wb3J0YW50O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/help/help.page.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/help/help.page.ts ***!
  \*****************************************/
/*! exports provided: HelpPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HelpPage", function() { return HelpPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");

/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/

let HelpPage = class HelpPage {
    constructor() { }
    ngOnInit() {
    }
};
HelpPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-help',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./help.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/help/help.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./help.page.scss */ "./src/app/pages/help/help.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], HelpPage);



/***/ })

}]);
//# sourceMappingURL=pages-help-help-module-es2015.js.map