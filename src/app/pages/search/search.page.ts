import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {
  forYou = [
    {
      img: 'https://i.pinimg.com/originals/32/b9/2d/32b92d0b5c648689e36a6fd0a40c0bbf.jpg',
      header: 'HANDBAGS',
      footer: '+ SHOP'
    },
    {
      img: 'https://i.pinimg.com/originals/d4/11/a2/d411a2d3d539a6934eb328c1b2d3c971.jpg',
      header: 'FOOTWEAR',
      footer: '+ FOR HER'
    },
    {
      img: 'https://static.bershka.net/4/photos2//2018/V/0/2/p/0264/251/400/0264251400_1_1_3.jpg?t=1514970277706',
      header: 'SPORTSWEAR',
      footer: '+ SHOP'
    },
    {
      img: 'https://rebajas2017.com/wp-content/uploads/2016/09/rebajas-bershka-2017-moda-hombre-sudaderas.jpg',
      header: 'FOOTWEAR',
      footer: '+ FOR HIM'
    },
    {
      img: 'https://i.pinimg.com/736x/af/2e/c7/af2ec75cfe1d250b88515c60fdfca4e0.jpg',
      header: 'ETHIC WEAR',
      footer: '+ SHOP'
    },
    {
      img: 'assets/shop/6.jpg',
      header: 'WESTERN WEAR',
      footer: '+ FOR HER'
    },
    {
      img: 'assets/shop/11.jpg',
      header: 'KIDS WEAR',
      footer: '+ SHOP'
    },
    {
      img: 'assets/shop/12.jpg',
      header: 'HOME',
      footer: '+ SHOP'
    },
    {
      img: 'assets/shop/23.jpg',
      header: 'More',
      footer: '+ SHOP'
    },
  ];
  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

  onCategorySelect(item) {
    console.log('item', item);
    //
    this.router.navigate(['main-category']);
  }

}
