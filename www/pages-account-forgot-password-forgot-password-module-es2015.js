(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-account-forgot-password-forgot-password-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/account/forgot-password/forgot-password.page.html":
/*!***************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/account/forgot-password/forgot-password.page.html ***!
  \***************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button mode=\"md\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Cambiar contraseña</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"div_content\">\n    <div class=\"div_logo\">\n      <img style=\"height: 250px; width: 250px;\" src=\"assets/images/logo_ropapp.png\" class=\"img_logo\" />\n    </div>\n\n    <div class=\"div_primary_text\" *ngIf=\"step==1\">\n      <div class=\"div_inner\">\n        <img src=\"assets/images/user_icon.png\" class=\"img_user\" />\n        <ion-input type=\"text\" Placeholder=\"Email Address\" class=\"in\"></ion-input>\n      </div>\n\n      <div class=\"div_btn\">\n        <ion-button color=\"primary\" class=\"btn_login\" expand=\"block\" mode=\"ios\" (click)=\"step = 2\">Enviar </ion-button>\n      </div>\n\n    </div>\n\n    <div class=\"div_primary_text\" *ngIf=\"step==2\">\n      <div class=\"div_inner\">\n        <img src=\"assets/images/user_icon.png\" class=\"img_user\" />\n        <ion-input type=\"text\" Placeholder=\"OTP\" class=\"in\"></ion-input>\n      </div>\n\n      <div class=\"div_btn\">\n        <ion-button class=\"btn_login\" expand=\"block\" mode=\"ios\" (click)=\"step = 3\">Verificar </ion-button>\n      </div>\n\n    </div>\n\n    <div class=\"div_primary_text\" *ngIf=\"step==3\">\n\n      <div class=\"div_inner\">\n        <img src=\"assets/images/lock.png\" class=\"img_user\" />\n        <ion-input type=\"password\" Placeholder=\"Password\" class=\"in\"></ion-input>\n      </div>\n      <div class=\"div_inner\">\n        <img src=\"assets/images/lock.png\" class=\"img_user\" />\n        <ion-input type=\"password\" Placeholder=\"Confirm Password\" class=\"in\"></ion-input>\n      </div>\n      <div class=\"div_btn\">\n        <ion-button class=\"btn_login\" expand=\"block\" mode=\"ios\" (click)=\"changes()\">Actualizar contraseña</ion-button>\n      </div>\n\n    </div>\n\n  </div>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/account/forgot-password/forgot-password.module.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/account/forgot-password/forgot-password.module.ts ***!
  \*************************************************************************/
/*! exports provided: ForgotPasswordPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForgotPasswordPageModule", function() { return ForgotPasswordPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _forgot_password_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./forgot-password.page */ "./src/app/pages/account/forgot-password/forgot-password.page.ts");







const routes = [
    {
        path: '',
        component: _forgot_password_page__WEBPACK_IMPORTED_MODULE_6__["ForgotPasswordPage"]
    }
];
let ForgotPasswordPageModule = class ForgotPasswordPageModule {
};
ForgotPasswordPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_forgot_password_page__WEBPACK_IMPORTED_MODULE_6__["ForgotPasswordPage"]]
    })
], ForgotPasswordPageModule);



/***/ }),

/***/ "./src/app/pages/account/forgot-password/forgot-password.page.scss":
/*!*************************************************************************!*\
  !*** ./src/app/pages/account/forgot-password/forgot-password.page.scss ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".div_content {\n  width: 100%;\n  height: 65vh;\n}\n.div_content .div_logo {\n  width: 100%;\n  height: 30vh;\n  position: relative;\n}\n.div_content .div_logo .img_logo {\n  width: 105px;\n  position: absolute;\n  left: 50%;\n  top: 40%;\n  -webkit-transform: translate(-50%, -50%);\n          transform: translate(-50%, -50%);\n}\n.div_content .div_segment {\n  width: 100%;\n  padding-left: 40px;\n  padding-right: 40px;\n}\n.div_content .div_segment .lbl_segment {\n  margin-bottom: 5px;\n  font-family: Exo2-Bold;\n}\n.div_content .div_segment ion-segment-button {\n  border-style: unset;\n  font-size: 14px;\n  text-transform: capitalize;\n}\n.div_content .div_segment ion-segment {\n  --background-hover: none !important;\n  --color: var(--ion-color-text);\n  --color-checked: var(--ion-color-primary);\n  --color-checked-disabled: var(--color-checked);\n  border-bottom: 1px solid lightgray;\n  --background-activated: transparent !important;\n  --background-checked: transparent !important;\n  margin-top: 0px;\n}\n.div_content .div_segment .segment-button-indicator {\n  background-color: var(--ion-color-primary);\n}\n.div_content .div_primary_text {\n  width: 100%;\n  padding-left: 20px;\n  padding-right: 20px;\n}\n.div_content .div_primary_text .div_inner {\n  margin-top: 10px;\n  height: 40px;\n  width: 100%;\n  border-radius: 5px;\n  background-color: #eeeeef;\n  position: relative;\n}\n.div_content .div_primary_text .div_inner .img_user {\n  width: 15px;\n  position: absolute;\n  left: 5%;\n  top: 50%;\n  -webkit-transform: translateY(-50%);\n          transform: translateY(-50%);\n}\n.div_content .div_primary_text .div_inner .in {\n  position: absolute;\n  width: 90%;\n  left: 15%;\n  top: 50%;\n  -webkit-transform: translateY(-50%);\n          transform: translateY(-50%);\n  font-size: 14px;\n}\n.div_content .div_primary_text .lbl_forgot {\n  font-size: 14px;\n  display: block;\n  text-align: center;\n  margin-top: 20px;\n}\n.div_content .div_primary_text .div_btn {\n  width: 100%;\n  margin-top: 20px;\n  position: relative;\n}\n.div_content .div_primary_text .div_btn .btn_login {\n  color: white;\n  --background: var(--ion-color-custom);\n  --border-radius: 5px;\n}\n.div_content .div_primary_text h2 {\n  overflow: hidden;\n  text-align: center;\n}\n.div_content .div_primary_text .div_or {\n  width: 100%;\n  margin-top: 20px;\n}\n.div_content .div_primary_text .div_or .lbl_or {\n  position: absolute;\n  left: 50%;\n  top: 50%;\n  -webkit-transform: translate(-50%, -50%);\n          transform: translate(-50%, -50%);\n  font-size: 15px;\n}\n.div_content .div_primary_text .div_or .img_line {\n  width: 100%;\n  height: 1px;\n  margin-top: 7px;\n  background-color: var(--ion-color-dark) !important;\n}\n.div_content .div_primary_text .div_social {\n  margin-top: 15px;\n}\n.div_content .div_primary_text .div_social .img_fb {\n  width: 50px;\n}\n.div_content .div_primary_text .div_social .img_twit {\n  width: 50px;\n}\n.div_content .div_primary_text .div_social .img_gplus {\n  width: 50px;\n}\n.div_content .div_primary_text .lbl_create {\n  text-align: center;\n  display: block;\n  font-weight: bold;\n  color: var(--ion-color-darl);\n  font-size: 16px;\n  margin-top: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYWNjb3VudC9mb3Jnb3QtcGFzc3dvcmQvQzpcXFVzZXJzXFxVc2VyXFxEb3dubG9hZHNcXGlvbmljNVNob3BBcHAtNXlscGRjXFxpb25pYzVTaG9wQXBwXFxBcHBfY29kZS9zcmNcXGFwcFxccGFnZXNcXGFjY291bnRcXGZvcmdvdC1wYXNzd29yZFxcZm9yZ290LXBhc3N3b3JkLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvYWNjb3VudC9mb3Jnb3QtcGFzc3dvcmQvZm9yZ290LXBhc3N3b3JkLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQTtFQWNJLFdBQUE7RUFDQSxZQUFBO0FDYko7QURESTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7QUNHUjtBRERRO0VBQ0ksWUFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLFFBQUE7RUFDQSx3Q0FBQTtVQUFBLGdDQUFBO0FDR1o7QURHSTtFQUNJLFdBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0FDRFI7QURHUTtFQUNJLGtCQUFBO0VBQ0Esc0JBQUE7QUNEWjtBREdRO0VBQ0ksbUJBQUE7RUFDQSxlQUFBO0VBQ0EsMEJBQUE7QUNEWjtBREdRO0VBQ0ksbUNBQUE7RUFDQSw4QkFBQTtFQUNBLHlDQUFBO0VBQ0EsOENBQUE7RUFDQSxrQ0FBQTtFQUNBLDhDQUFBO0VBQ0EsNENBQUE7RUFDQSxlQUFBO0FDRFo7QURHUTtFQUNJLDBDQUFBO0FDRFo7QURLSTtFQUNJLFdBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0FDSFI7QURLUTtFQUNJLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7QUNIWjtBREtZO0VBQ0ksV0FBQTtFQUNBLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFFBQUE7RUFDQSxtQ0FBQTtVQUFBLDJCQUFBO0FDSGhCO0FES1k7RUFDSSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxTQUFBO0VBQ0EsUUFBQTtFQUNBLG1DQUFBO1VBQUEsMkJBQUE7RUFDQSxlQUFBO0FDSGhCO0FETVE7RUFDSSxlQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7QUNKWjtBRE1RO0VBQ0ksV0FBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7QUNKWjtBRE1ZO0VBQ0ksWUFBQTtFQUNBLHFDQUFBO0VBQ0Esb0JBQUE7QUNKaEI7QURPUTtFQUNJLGdCQUFBO0VBQ0Esa0JBQUE7QUNMWjtBRFNRO0VBQ0ksV0FBQTtFQUNBLGdCQUFBO0FDUFo7QURTWTtFQUNJLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLFFBQUE7RUFDQSx3Q0FBQTtVQUFBLGdDQUFBO0VBQ0EsZUFBQTtBQ1BoQjtBRFNZO0VBQ0ksV0FBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0Esa0RBQUE7QUNQaEI7QURVUTtFQUNJLGdCQUFBO0FDUlo7QURTWTtFQUNJLFdBQUE7QUNQaEI7QURVWTtFQUNJLFdBQUE7QUNSaEI7QURVWTtFQUNJLFdBQUE7QUNSaEI7QURZUTtFQUNJLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsNEJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUNWWiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2FjY291bnQvZm9yZ290LXBhc3N3b3JkL2ZvcmdvdC1wYXNzd29yZC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcbi5kaXZfY29udGVudHtcbiAgICAuZGl2X2xvZ297XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBoZWlnaHQ6IDMwdmg7XG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcblxuICAgICAgICAuaW1nX2xvZ297XG4gICAgICAgICAgICB3aWR0aDogMTA1cHg7XG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgICAgICBsZWZ0OiA1MCU7XG4gICAgICAgICAgICB0b3A6IDQwJTtcbiAgICAgICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsLTUwJSk7XG4gICAgICAgIH1cbiAgICB9XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiA2NXZoO1xuXG4gICAgLmRpdl9zZWdtZW50e1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgcGFkZGluZy1sZWZ0OiA0MHB4O1xuICAgICAgICBwYWRkaW5nLXJpZ2h0OiA0MHB4O1xuXG4gICAgICAgIC5sYmxfc2VnbWVudHtcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDVweDtcbiAgICAgICAgICAgIGZvbnQtZmFtaWx5OiBFeG8yLUJvbGQ7XG4gICAgICAgIH1cbiAgICAgICAgaW9uLXNlZ21lbnQtYnV0dG9ue1xuICAgICAgICAgICAgYm9yZGVyLXN0eWxlIDogdW5zZXQ7XG4gICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbiAgICAgICAgfVxuICAgICAgICBpb24tc2VnbWVudHtcbiAgICAgICAgICAgIC0tYmFja2dyb3VuZC1ob3ZlciA6IG5vbmUgIWltcG9ydGFudDtcbiAgICAgICAgICAgIC0tY29sb3IgOiB2YXIoLS1pb24tY29sb3ItdGV4dCk7XG4gICAgICAgICAgICAtLWNvbG9yLWNoZWNrZWQgOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG4gICAgICAgICAgICAtLWNvbG9yLWNoZWNrZWQtZGlzYWJsZWQgOiB2YXIoLS1jb2xvci1jaGVja2VkKTtcbiAgICAgICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBsaWdodGdyYXk7XG4gICAgICAgICAgICAtLWJhY2tncm91bmQtYWN0aXZhdGVkOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50OyBcbiAgICAgICAgICAgIC0tYmFja2dyb3VuZC1jaGVja2VkOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50O1xuICAgICAgICAgICAgbWFyZ2luLXRvcDogMHB4O1xuICAgICAgICB9XG4gICAgICAgIC5zZWdtZW50LWJ1dHRvbi1pbmRpY2F0b3J7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yICA6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIC5kaXZfcHJpbWFyeV90ZXh0e1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAyMHB4O1xuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAyMHB4O1xuXG4gICAgICAgIC5kaXZfaW5uZXJ7XG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xuICAgICAgICAgICAgaGVpZ2h0OiA0MHB4O1xuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWVlZWVmO1xuICAgICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuXG4gICAgICAgICAgICAuaW1nX3VzZXJ7XG4gICAgICAgICAgICAgICAgd2lkdGg6IDE1cHg7XG4gICAgICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICAgICAgICAgIGxlZnQ6IDUlO1xuICAgICAgICAgICAgICAgIHRvcDogNTAlO1xuICAgICAgICAgICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgtNTAlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC5pbntcbiAgICAgICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgICAgICAgICAgd2lkdGg6IDkwJTtcbiAgICAgICAgICAgICAgICBsZWZ0OiAxNSU7XG4gICAgICAgICAgICAgICAgdG9wOiA1MCU7XG4gICAgICAgICAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICAubGJsX2ZvcmdvdHtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICAgICAgbWFyZ2luLXRvcDogMjBweDtcbiAgICAgICAgfVxuICAgICAgICAuZGl2X2J0bntcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAgICAgbWFyZ2luLXRvcDogMjBweDtcbiAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcblxuICAgICAgICAgICAgLmJ0bl9sb2dpbntcbiAgICAgICAgICAgICAgICBjb2xvcjp3aGl0ZTtcbiAgICAgICAgICAgICAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1jdXN0b20pO1xuICAgICAgICAgICAgICAgIC0tYm9yZGVyLXJhZGl1czogNXB4O1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIGgyIHtcbiAgICAgICAgICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIH1cblxuICAgICAgIFxuICAgICAgICAuZGl2X29ye1xuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAyMHB4O1xuXG4gICAgICAgICAgICAubGJsX29ye1xuICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICAgICAgICBsZWZ0OiA1MCU7XG4gICAgICAgICAgICAgICAgdG9wOiA1MCU7XG4gICAgICAgICAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwtNTAlKTtcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE1cHg7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAuaW1nX2xpbmV7XG4gICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgICAgICAgICAgaGVpZ2h0OiAxcHg7XG4gICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogN3B4O1xuICAgICAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6dmFyKC0taW9uLWNvbG9yLWRhcmspICFpbXBvcnRhbnQ7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgLmRpdl9zb2NpYWx7XG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAxNXB4O1xuICAgICAgICAgICAgLmltZ19mYntcbiAgICAgICAgICAgICAgICB3aWR0aDogNTBweDtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC5pbWdfdHdpdHtcbiAgICAgICAgICAgICAgICB3aWR0aDogNTBweDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC5pbWdfZ3BsdXN7XG4gICAgICAgICAgICAgICAgd2lkdGg6IDUwcHg7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICAubGJsX2NyZWF0ZXtcbiAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLWRhcmwpOztcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XG4gICAgICAgIH1cbiAgICB9XG59IiwiLmRpdl9jb250ZW50IHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogNjV2aDtcbn1cbi5kaXZfY29udGVudCAuZGl2X2xvZ28ge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAzMHZoO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4uZGl2X2NvbnRlbnQgLmRpdl9sb2dvIC5pbWdfbG9nbyB7XG4gIHdpZHRoOiAxMDVweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiA1MCU7XG4gIHRvcDogNDAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcbn1cbi5kaXZfY29udGVudCAuZGl2X3NlZ21lbnQge1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZy1sZWZ0OiA0MHB4O1xuICBwYWRkaW5nLXJpZ2h0OiA0MHB4O1xufVxuLmRpdl9jb250ZW50IC5kaXZfc2VnbWVudCAubGJsX3NlZ21lbnQge1xuICBtYXJnaW4tYm90dG9tOiA1cHg7XG4gIGZvbnQtZmFtaWx5OiBFeG8yLUJvbGQ7XG59XG4uZGl2X2NvbnRlbnQgLmRpdl9zZWdtZW50IGlvbi1zZWdtZW50LWJ1dHRvbiB7XG4gIGJvcmRlci1zdHlsZTogdW5zZXQ7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG59XG4uZGl2X2NvbnRlbnQgLmRpdl9zZWdtZW50IGlvbi1zZWdtZW50IHtcbiAgLS1iYWNrZ3JvdW5kLWhvdmVyOiBub25lICFpbXBvcnRhbnQ7XG4gIC0tY29sb3I6IHZhcigtLWlvbi1jb2xvci10ZXh0KTtcbiAgLS1jb2xvci1jaGVja2VkOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG4gIC0tY29sb3ItY2hlY2tlZC1kaXNhYmxlZDogdmFyKC0tY29sb3ItY2hlY2tlZCk7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBsaWdodGdyYXk7XG4gIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6IHRyYW5zcGFyZW50ICFpbXBvcnRhbnQ7XG4gIC0tYmFja2dyb3VuZC1jaGVja2VkOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50O1xuICBtYXJnaW4tdG9wOiAwcHg7XG59XG4uZGl2X2NvbnRlbnQgLmRpdl9zZWdtZW50IC5zZWdtZW50LWJ1dHRvbi1pbmRpY2F0b3Ige1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG59XG4uZGl2X2NvbnRlbnQgLmRpdl9wcmltYXJ5X3RleHQge1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZy1sZWZ0OiAyMHB4O1xuICBwYWRkaW5nLXJpZ2h0OiAyMHB4O1xufVxuLmRpdl9jb250ZW50IC5kaXZfcHJpbWFyeV90ZXh0IC5kaXZfaW5uZXIge1xuICBtYXJnaW4tdG9wOiAxMHB4O1xuICBoZWlnaHQ6IDQwcHg7XG4gIHdpZHRoOiAxMDAlO1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlZWVlZWY7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5kaXZfY29udGVudCAuZGl2X3ByaW1hcnlfdGV4dCAuZGl2X2lubmVyIC5pbWdfdXNlciB7XG4gIHdpZHRoOiAxNXB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDUlO1xuICB0b3A6IDUwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xufVxuLmRpdl9jb250ZW50IC5kaXZfcHJpbWFyeV90ZXh0IC5kaXZfaW5uZXIgLmluIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB3aWR0aDogOTAlO1xuICBsZWZ0OiAxNSU7XG4gIHRvcDogNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTUwJSk7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cbi5kaXZfY29udGVudCAuZGl2X3ByaW1hcnlfdGV4dCAubGJsX2ZvcmdvdCB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLXRvcDogMjBweDtcbn1cbi5kaXZfY29udGVudCAuZGl2X3ByaW1hcnlfdGV4dCAuZGl2X2J0biB7XG4gIHdpZHRoOiAxMDAlO1xuICBtYXJnaW4tdG9wOiAyMHB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4uZGl2X2NvbnRlbnQgLmRpdl9wcmltYXJ5X3RleHQgLmRpdl9idG4gLmJ0bl9sb2dpbiB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItY3VzdG9tKTtcbiAgLS1ib3JkZXItcmFkaXVzOiA1cHg7XG59XG4uZGl2X2NvbnRlbnQgLmRpdl9wcmltYXJ5X3RleHQgaDIge1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uZGl2X2NvbnRlbnQgLmRpdl9wcmltYXJ5X3RleHQgLmRpdl9vciB7XG4gIHdpZHRoOiAxMDAlO1xuICBtYXJnaW4tdG9wOiAyMHB4O1xufVxuLmRpdl9jb250ZW50IC5kaXZfcHJpbWFyeV90ZXh0IC5kaXZfb3IgLmxibF9vciB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogNTAlO1xuICB0b3A6IDUwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XG4gIGZvbnQtc2l6ZTogMTVweDtcbn1cbi5kaXZfY29udGVudCAuZGl2X3ByaW1hcnlfdGV4dCAuZGl2X29yIC5pbWdfbGluZSB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDFweDtcbiAgbWFyZ2luLXRvcDogN3B4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItZGFyaykgIWltcG9ydGFudDtcbn1cbi5kaXZfY29udGVudCAuZGl2X3ByaW1hcnlfdGV4dCAuZGl2X3NvY2lhbCB7XG4gIG1hcmdpbi10b3A6IDE1cHg7XG59XG4uZGl2X2NvbnRlbnQgLmRpdl9wcmltYXJ5X3RleHQgLmRpdl9zb2NpYWwgLmltZ19mYiB7XG4gIHdpZHRoOiA1MHB4O1xufVxuLmRpdl9jb250ZW50IC5kaXZfcHJpbWFyeV90ZXh0IC5kaXZfc29jaWFsIC5pbWdfdHdpdCB7XG4gIHdpZHRoOiA1MHB4O1xufVxuLmRpdl9jb250ZW50IC5kaXZfcHJpbWFyeV90ZXh0IC5kaXZfc29jaWFsIC5pbWdfZ3BsdXMge1xuICB3aWR0aDogNTBweDtcbn1cbi5kaXZfY29udGVudCAuZGl2X3ByaW1hcnlfdGV4dCAubGJsX2NyZWF0ZSB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLWRhcmwpO1xuICBmb250LXNpemU6IDE2cHg7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG59Il19 */");

/***/ }),

/***/ "./src/app/pages/account/forgot-password/forgot-password.page.ts":
/*!***********************************************************************!*\
  !*** ./src/app/pages/account/forgot-password/forgot-password.page.ts ***!
  \***********************************************************************/
/*! exports provided: ForgotPasswordPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForgotPasswordPage", function() { return ForgotPasswordPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");

/*

  Authors : initappz (Rahul Jograna)
  Website : https://initappz.com/
  Created : 17-March-2020
  This App Template Source code is licensed as per the
  terms found in the Website https://initappz.com/license
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/


let ForgotPasswordPage = class ForgotPasswordPage {
    constructor(navCtrl) {
        this.navCtrl = navCtrl;
        this.step = 1;
    }
    ngOnInit() {
    }
    changes() {
        this.navCtrl.back();
    }
};
ForgotPasswordPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] }
];
ForgotPasswordPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-forgot-password',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./forgot-password.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/account/forgot-password/forgot-password.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./forgot-password.page.scss */ "./src/app/pages/account/forgot-password/forgot-password.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]])
], ForgotPasswordPage);



/***/ })

}]);
//# sourceMappingURL=pages-account-forgot-password-forgot-password-module-es2015.js.map