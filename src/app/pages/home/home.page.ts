/*

  Authors : (Xavier Siavichay & Josue Vargas)
  Website : https://initappz.com/
  Created : 17-March-2020
  Copyright and Good Faith Purchasers © 2020-present initappz.

*/
import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { Router } from '@angular/router';
import { DummyDataService } from 'src/app/services/dummy-data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  @ViewChild('mainSlider', { static: false }) mainSlider: IonSlides;


  slidesOptsHeader = {
    slidesPerView: 4,
  };
  slidesOptsProduct1 = {
    slidesPerView: 1.3,
  };

  img: any = 'https://m.zeleb.es/sites/default/files/esta_noche_te_iras_de_fiesta_con_ropa_de_bershka_y_hm_es_un_nuevo_mandamiento.jpg';
  img2: any = 'https://estaticos.efe.com/efecom/recursos2/imagen.aspx?lVW2oAh2vjO1-P-2fHQqc30OJTzIirbD2HatQ4TncnkXVSTX-P-2bAoG0sxzXPZPAk5l-P-2fU5UzjGylKWxGo9qRqiBrS1vRA-P-3d-P-3d';

  mainSliders = [];
  productSliderOne = [];
  forYou = [];
  player = [];
  otro = [];
  constructor(
    private router: Router,
    private dummy: DummyDataService
  ) {
    this.mainSliders = this.dummy.catSlider;
    this.productSliderOne = this.dummy.productSlider;
    this.forYou = this.dummy.specialForYou;
    this.player = this.dummy.playera;
    this.otro = this.dummy.otro;
  }

  ngOnInit() {
  }
  onSearch() {
    console.log('search page');
  }
  onNotification() {
    console.log('notification page');
  }
  onBookmark() {
    console.log('bookmark page');
  }
  onCart() {
    console.log('cart page');
  }

  onCategorySelect(item) {
    console.log('item', item);
    this.router.navigate(['main-category']);
  }
}
