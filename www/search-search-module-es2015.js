(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["search-search-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/search/search.page.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/search/search.page.html ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-searchbar placeholder=\"Search.\" mode=\"ios\"></ion-searchbar>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n\n  <ion-grid fixed>\n    <ion-row>\n      <ion-col size=\"4\" *ngFor=\"let item of forYou\">\n        <div class=\"main_frame\" (click)=\"onCategorySelect(item)\">\n          <div [style.backgroundImage]=\"'url('+item.img+')'\" class=\"product_img\"></div>\n          <p class=\"header_title\">{{item.header}}</p>\n          <p class=\"footer_title\">{{item.footer}}</p>\n        </div>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  \n</ion-content>");

/***/ }),

/***/ "./src/app/pages/search/search.module.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/search/search.module.ts ***!
  \***********************************************/
/*! exports provided: SearchPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchPageModule", function() { return SearchPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _search_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./search.page */ "./src/app/pages/search/search.page.ts");







const routes = [
    {
        path: '',
        component: _search_page__WEBPACK_IMPORTED_MODULE_6__["SearchPage"]
    }
];
let SearchPageModule = class SearchPageModule {
};
SearchPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_search_page__WEBPACK_IMPORTED_MODULE_6__["SearchPage"]]
    })
], SearchPageModule);



/***/ }),

/***/ "./src/app/pages/search/search.page.scss":
/*!***********************************************!*\
  !*** ./src/app/pages/search/search.page.scss ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".main_frame .product_img {\n  height: 100px;\n  width: 100%;\n  padding: 5px;\n  border: 1px solid lightgray;\n  background-repeat: no-repeat;\n  background-position: center;\n  background-size: cover;\n}\n.main_frame .header_title {\n  font-size: 10px;\n  font-weight: bold;\n  color: black;\n}\n.main_frame .footer_title {\n  font-size: 8px;\n  color: gray;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvc2VhcmNoL0M6XFxVc2Vyc1xcVXNlclxcRG93bmxvYWRzXFxpb25pYzVTaG9wQXBwLTV5bHBkY1xcaW9uaWM1U2hvcEFwcFxcQXBwX2NvZGUvc3JjXFxhcHBcXHBhZ2VzXFxzZWFyY2hcXHNlYXJjaC5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL3NlYXJjaC9zZWFyY2gucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNJO0VBQ0ksYUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsMkJBQUE7RUFFQSw0QkFBQTtFQUNBLDJCQUFBO0VBQ0Esc0JBQUE7QUNEUjtBREdJO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0VBQ0EsWUFBQTtBQ0RSO0FER0k7RUFDSSxjQUFBO0VBQ0EsV0FBQTtBQ0RSIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvc2VhcmNoL3NlYXJjaC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubWFpbl9mcmFtZXtcbiAgICAucHJvZHVjdF9pbWd7XG4gICAgICAgIGhlaWdodDogMTAwcHg7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBwYWRkaW5nOiA1cHg7XG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkIGxpZ2h0Z3JheTtcblxuICAgICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gICAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgfVxuICAgIC5oZWFkZXJfdGl0bGV7XG4gICAgICAgIGZvbnQtc2l6ZTogMTBweDtcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgICAgIGNvbG9yOiBibGFjaztcbiAgICB9XG4gICAgLmZvb3Rlcl90aXRsZXtcbiAgICAgICAgZm9udC1zaXplOiA4cHg7XG4gICAgICAgIGNvbG9yOiBncmF5O1xuICAgIH1cbn0iLCIubWFpbl9mcmFtZSAucHJvZHVjdF9pbWcge1xuICBoZWlnaHQ6IDEwMHB4O1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZzogNXB4O1xuICBib3JkZXI6IDFweCBzb2xpZCBsaWdodGdyYXk7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3Zlcjtcbn1cbi5tYWluX2ZyYW1lIC5oZWFkZXJfdGl0bGUge1xuICBmb250LXNpemU6IDEwcHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogYmxhY2s7XG59XG4ubWFpbl9mcmFtZSAuZm9vdGVyX3RpdGxlIHtcbiAgZm9udC1zaXplOiA4cHg7XG4gIGNvbG9yOiBncmF5O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/search/search.page.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/search/search.page.ts ***!
  \*********************************************/
/*! exports provided: SearchPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchPage", function() { return SearchPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



let SearchPage = class SearchPage {
    constructor(router) {
        this.router = router;
        this.forYou = [
            {
                img: 'https://i.pinimg.com/originals/32/b9/2d/32b92d0b5c648689e36a6fd0a40c0bbf.jpg',
                header: 'HANDBAGS',
                footer: '+ SHOP'
            },
            {
                img: 'https://i.pinimg.com/originals/d4/11/a2/d411a2d3d539a6934eb328c1b2d3c971.jpg',
                header: 'FOOTWEAR',
                footer: '+ FOR HER'
            },
            {
                img: 'https://static.bershka.net/4/photos2//2018/V/0/2/p/0264/251/400/0264251400_1_1_3.jpg?t=1514970277706',
                header: 'SPORTSWEAR',
                footer: '+ SHOP'
            },
            {
                img: 'https://rebajas2017.com/wp-content/uploads/2016/09/rebajas-bershka-2017-moda-hombre-sudaderas.jpg',
                header: 'FOOTWEAR',
                footer: '+ FOR HIM'
            },
            {
                img: 'https://i.pinimg.com/736x/af/2e/c7/af2ec75cfe1d250b88515c60fdfca4e0.jpg',
                header: 'ETHIC WEAR',
                footer: '+ SHOP'
            },
            {
                img: 'assets/shop/6.jpg',
                header: 'WESTERN WEAR',
                footer: '+ FOR HER'
            },
            {
                img: 'assets/shop/11.jpg',
                header: 'KIDS WEAR',
                footer: '+ SHOP'
            },
            {
                img: 'assets/shop/12.jpg',
                header: 'HOME',
                footer: '+ SHOP'
            },
            {
                img: 'assets/shop/23.jpg',
                header: 'More',
                footer: '+ SHOP'
            },
        ];
    }
    ngOnInit() {
    }
    onCategorySelect(item) {
        console.log('item', item);
        //
        this.router.navigate(['main-category']);
    }
};
SearchPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
SearchPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-search',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./search.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/search/search.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./search.page.scss */ "./src/app/pages/search/search.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
], SearchPage);



/***/ })

}]);
//# sourceMappingURL=search-search-module-es2015.js.map